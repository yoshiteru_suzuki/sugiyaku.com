# Movable Type (r) (C) 2005-2012 Six Apart, Ltd. All Rights Reserved.
# This code cannot be redistributed without permission from www.sixapart.com.
# For more information, consult your Movable Type license.
#
# $Id$

package TypePadAntiSpam::L10N::en_us;

use strict;

use base 'TypePadAntiSpam::L10N';
use vars qw( %Lexicon );
%Lexicon = ( 

## plugins/TypePadAntiSpam/tmpl/stats_widget.tmpl 
	'widget_label_width' => '57', 
	'widget_totals_width' => '200', 
);
1;
