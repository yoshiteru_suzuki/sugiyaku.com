$(function() {

	var $form = $('.form-yoyaku'),
	$skypeid = $('input[name="skypeid"]'),
	$hajimetenote = $('.hajimete-note');

	$('#sex').on('change', function(){
		if($(this).val() == "男性")
			$('.box-women').hide();
			else {
				$('.box-women').show();
			}
	});

	$('input[name="houhou"]').on('change', function () {
		if($(this).val() == "スカイプ"){
			$skypeid.prop('required', true);
			$skypeid.parent().addClass('required');
		}
		else {
			$skypeid.prop('required', false);
			$skypeid.parent().removeClass('required');
		}
	});
	$('input[name="hajimete"]').on('change', function(){
		if($(this).val() != "2回目以降"){
			$hajimetenote.removeClass('show');
			$('.sex, .age, .adr').addClass('required');
			$('.sex, .age, .adr').find('input, select').prop('required', true);
		}
		else {
			$hajimetenote.addClass('show');
			$('.sex, .age, .adr').removeClass('required');
			$('.sex, .age, .adr').find('input, select').prop('required', false);
		}
	});

	$('input[name="target"]').on('change', function () {
		if($(this).val() == "ペット"){
			$('.pet').show();
			$('.no-pet').hide();
		}
		else {
			$('.pet').hide();
			$('.no-pet').show();
		}
	});

	$form.submit(function () {
		val = $('input[name="name"]').val();
		val += $('input[name="address-level2"]').val();
		val += $('input[name="address-level2"]').val();

		return true;
		if (includeJa(val)) {
			return true;
		} else {
			alert('エラーにより送信できませんでした。お手数おかけしますが、営業時間内にお電話でお問い合わせください。');
			return false;
		}
	});

console.log("1");
	document.getElementById('button-submit').addEventListener('click', function(event) {

		// フォームが有効かを確認する
		if (!document.getElementById('form-yoyaku').checkValidity()) {
				// デフォルトの送信動作をキャンセル
				event.preventDefault();
				
				// 最初のエラー要素を見つける
			var invalidFields = document.getElementById('form-yoyaku').querySelectorAll(':invalid');
				if (invalidFields.length > 0) {
						var firstInvalidField = invalidFields[0];
						
						// スクロールして該当箇所にフォーカスする
						firstInvalidField.scrollIntoView({ behavior: 'smooth' });
						firstInvalidField.focus();
						
						// エラー要素にスタイルを適用
						firstInvalidField.classList.add('error');
				}
		}
});


});


function includeJa(text) {
	try {
			var gmi = 'gmi';
			var regeIncludeHiragana = '^(?=.*[\u3041-\u3096]).*$';
			var regeIncludeKatakana = '^(?=.*[\u30A1-\u30FA]).*$';
			var regeIncludeKanji = '^(?=.*[\u4E00-\u9FFF]).*$';
			var regeHiragana = new RegExp(regeIncludeHiragana, gmi);
			var regeKatakana = new RegExp(regeIncludeKatakana, gmi);
			var regeKanji = new RegExp(regeIncludeKanji, gmi);

			var includeJa = false;
			if (regeHiragana.test(text))
					includeJa = true;
			if (regeKatakana.test(text))
					includeJa = true;
			if (regeKanji.test(text))
					includeJa = true;

			return includeJa;
	} catch (error) {
			alert(error);
	}
}
