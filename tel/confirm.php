<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>電話・Skype相談予約 | 漢方相談 スギヤマ薬局</title>
<?php readfile("../head.html")?>
</head>
<body id="confirm" class="under">
<div id="container">
<?php readfile("../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>電話・Skype相談予約</span></p>
		<div id="mainContents">

			<form action="index.php" method="post" id="form-order">
				<p class="mb20">下記内容をご確認の上送信してください。</p>
				<input type="hidden" name="name" value="<?php echo $DISP["name"]?>" />
				<input type="hidden" name="name_kana" value="<?php echo $DISP["name_kana"]?>" />
				<input type="hidden" name="tel1" value="<?php echo $DISP["tel1"]?>" />
				<input type="hidden" name="e_mail" value="<?php echo $DISP["e_mail"]?>" />
				<input type="hidden" name="year" value="<?php echo $DISP["year"]?>" />
				<input type="hidden" name="month" value="<?php echo $DISP["month"]?>" />
				<input type="hidden" name="day" value="<?php echo $DISP["day"]?>" />
				<input type="hidden" name="time" value="<?php echo $DISP["time"]?>" />
				<input type="hidden" name="kiboubi" value="<?php echo $DISP["year"]."年".$DISP["month"]."月".$DISP["day"]."日 ".$DISP["time"] ?>" />
				<input type="hidden" name="remarks" value="<?php echo $DISP["remarks"]?>" />


				<table class="formTbl01">
				<tr>
				<th>お名前</th>
				<td><?php if( $DISP["name"] ){ echo $DISP["name"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>フリガナ</th>
				<td><?php if( $DISP["name_kana"] ){ echo $DISP["name_kana"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>電話番号</th>
				<td><?php if( $DISP["tel1"] ){ echo $DISP["tel1"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>E-Mail</th>
				<td><?php if( $DISP["e_mail"] ){ echo $DISP["e_mail"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>ご希望日時</th>
				<td>
					<?php echo $DISP["year"]."年".$DISP["month"]."月".$DISP["day"]."日 ".$DISP["time"] ?>
				</td>
				<tr>
				<th>備考</th>
				<td><?php if( $DISP["remarks"] ){ echo nl2br($DISP["remarks"]); }else{ echo "&nbsp"; } ?></textarea></td>
				</tr>
				<tr>
				</tr>
				</table>
				<input type="hidden" name="action" value="complete" />

				<div class="btn_submit">
					<input type="submit" name="submit[back]" value="入力内容を修正" class="input_b"   />
					<input type="submit" name="submit[done]" value="上記の内容で送信" class="input_b"   />
				</div>
			</form>

		</div>
		<!--mainContents-->

		<div id="subContents">
<?php readfile("../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../footer.html")?>
</div>
</body>
</html>
