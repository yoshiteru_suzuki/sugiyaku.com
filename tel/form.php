<?php
    // import
	require_once("mod_dateselectgenerator.php");

	$today = getdate();
	$addDays = 2;
	$baseSec = mktime(0, 0, 0, $today[mon], $today[mday], $today[year]);//基準日を秒で取得
	$addSec = $addDays * 86400;//日数×１日の秒数
	$targetSec = $baseSec + $addSec;


	if(empty($DISP["year"]))
	{
		$defaut_y = date("Y", $targetSec);
		$defaut_m = date("n", $targetSec);
		$defaut_d = date("j", $targetSec);
	}
	else
	{
		$defaut_y = $DISP["year"];
		$defaut_m = $DISP["month"];
		$defaut_d = $DISP["day"];

	}

	$dateGenerator = new dateSelectGenerator
	(array(
		"defaultYear" => $defaut_y,
		"defaultMonth" => $defaut_m,
		"defaultDay" => $defaut_d,
		"yearRange_from" => 0,
		"yearRange_to" => 1
	));

//	$dateGenerator = new dateSelectGenerator(1);


?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>電話・スカイプ相談 | 漢方相談 スギヤマ薬局</title>
<?php readfile("../head.html")?>
<link rel="stylesheet" href="/common/lib/jquery.validationEngine/validationEngine.jquery.css" type="text/css">
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine-ja.js" type="text/javascript"></script>
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
<script>
$(function() {
	$("form").validationEngine();
});
</script>
</head>
<body id="idx" class="under">
<div id="container">
<?php readfile("../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><a href="./step.html">電話・スカイプ相談</a><span class="sp">&gt;</span><span>予約フォーム</span></p>
		<div id="mainContents">
			<!-- <div id="main_visual">
				<h1 id="main_ttl"><img src="images/idx_ttl.png" alt="電話相談 お電話での健康相談も行っております。" /></h1>
				<div id="main_pic"><img src="images/idx_visual.jpg" alt="電話相談" /></div>
			</div> -->
			<!-- <section class="cont box1" id="skype">
				<p>
					Skypeの方はこちらからおかけください。
				</p>
				<script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
				<div id="SkypeButton_Call_takuyakanpo_1">
				 <script type="text/javascript">
				 Skype.ui({
				 "name": "call",
				 "element": "SkypeButton_Call_takuyakanpo_1",
				 "participants": ["takuyakanpo"],
				 "imageSize": 24
				 });
				 </script>
				</div>
				<ul>
					<li><a href="https://www.skype.com/" target="_blank">Skypeのダウンロード（公式サイト）</a></li>
					<li><a href="http://appllio.com/how-to-use-skype" target="_blank">Skypeの使い方（参考サイト）</a></li>
				</ul>
			</section> -->
			<!-- <section class="cont" id="denwa">
				<h1>お電話で予約する</h1>
				<p>電話番号　042-746-1951</p>
				<p>受付時間　9:00～19:00（日曜・祝祭日を除く）</p>
			</section> -->

			<section class="cont mt30 mb20" id="mail">
				<!-- <h1>メールで予約する<a href="/php/policy.html">個人情報の取り扱い &raquo;</a></h1> -->
				<h1>相談予約フォーム</h1>
				<ul>
				<li>下記、ご記入の上送信してください。</li>
				<li>自動返信メールが届きます。</li>
				<li>ご指定のお時間にすでにご予約が入っている場合は、事前にご連絡させていただきます。</li>
				</ul>
				<p class="mt10"><a href="/php/policy.html">個人情報の取り扱い &raquo;</a></p>
				<p class="mt10">
					<span class="fb">携帯、スマートフォンをご利用の方へ</span><br>迷惑メール防止のためメールの受信設定をしている場合は､「@sugiyaku.com」を登録、またはドメイン指定解除を行ってください｡<br>
				</p>
			</section>

			<form action="index.php" method="post">
				<p><b class="hiss">*</b> は必須項目です。</p>
				<table class="formTbl01">
				<tr>
				<th>お名前<b class="hiss">*</b></th>
				<td><input type="text" name="name" value="<?php echo $DISP["name"] ?>" class="validate[required]" title="お名前を入力してください"  /></td></tr>
				<tr><th>フリガナ<b class="hiss">*</b></th>
				<td><input type="text" name="name_kana" value="<?php echo $DISP["name_kana"] ?>" class="validate[required]" title="フリガナを入力してください" maxlength="20"  /></td>
				</tr>
				<tr>
				<th>電話番号<b class="hiss">*</b></th>
				<td>
					<input type="text" name="tel1" value="<?php echo $DISP["tel1"] ?>" class="validate[required]"  maxlength="20"  />
					<p>
						※Skypeの方はSkype IDを入力してください。
					</p>
				</td>
				</tr>
				<tr>
				<th>E-Mail<b class="hiss">*</b></th>
				<td><input type="text" name="e_mail" value="<?php echo $DISP["e_mail"] ?>" class="validate[required, custom[email]]" title="E-Mailを正しく入力してください"  maxlength="100"  /></td>
				</tr>
				<tr>
				<th>ご希望日時<b class="hiss">*</b></th>
				<td>
					<p>本日より2日後以降でお願いします。<br />お急ぎの場合はお電話にてお問い合わせください。</p>
					<!-- year -->
					<select name="year">
					<?php $dateGenerator->yearOptions() ?>
					</select>

					<!-- month -->
					<select name="month">
					<?php $dateGenerator->monthOptions() ?>
					</select>

					<!-- day -->
					<select name="day">
					<?php $dateGenerator->dayOptions() ?>
					</select>

					<!-- time -->
					<select name="time">
					<option value="10:00～" <?php if( strcmp($DISP["time"], "10:00～") == 0 ){ echo "selected"; } ?>>10:00～</option>
					<option value="11:00～" <?php if( strcmp($DISP["time"], "11:00～") == 0 ){ echo "selected"; } ?>>11:00～</option>
					<option value="12:00～" <?php if( strcmp($DISP["time"], "12:00～") == 0 ){ echo "selected"; } ?>>12:00～</option>
					<option value="13:00～" <?php if( strcmp($DISP["time"], "13:00～") == 0 ){ echo "selected"; } ?>>13:00～</option>
					<option value="14:00～" <?php if( strcmp($DISP["time"], "14:00～") == 0 ){ echo "selected"; } ?>>14:00～</option>
					<option value="15:00～" <?php if( strcmp($DISP["time"], "15:00～") == 0 ){ echo "selected"; } ?>>15:00～</option>
					<option value="16:00～" <?php if( strcmp($DISP["time"], "16:00～") == 0 ){ echo "selected"; } ?>>16:00～</option>
					<option value="17:00～" <?php if( strcmp($DISP["time"], "17:00～") == 0 ){ echo "selected"; } ?>>17:00～</option>
					<option value="18:00～" <?php if( strcmp($DISP["time"], "18:00～") == 0 ){ echo "selected"; } ?>>18:00～</option>
					<option value="19:00～" <?php if( strcmp($DISP["time"], "19:00～") == 0 ){ echo "selected"; } ?>>19:00～</option>
					</select>
				</td>
				</tr>
				<tr>
				<th>備考</th>
				<td>
					<textarea name="remarks" rows="3"><?php echo $DISP["remarks"] ?></textarea>
					<p>
						相談・お問い合わせ内容についてお書きください。
					</p>
				</td>
				</tr>
				<tr>
				</tr>
				</table>

				<input type="hidden" name="action" value="confirm" />

				<div class="btn_submit"><input type="submit" name="submit" value="確認画面へ進む" class="input_b"   /></div>
			</form>


		</div>
		<!--mainContents-->

		<div id="subContents">
<?php readfile("../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../footer.html")?>
</div>
</body>
</html>
