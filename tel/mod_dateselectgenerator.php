<?php

	class dateSelectGenerator
	{
		private $yearRange_from;
		private $yearRange_to;
		private $isFixedYear;
		private $isSelectCurrentDate;
		private $defaultYear;
		private $defaultMonth;
		private $defaultDay;
		private $currentYear;
		private $currentMonth;
		private $currentDay;
		
		function __construct($settings)
		{
			$this->currentYear = date(Y);
			$this->currentMonth = date(n);
			$this->currentDay = date(d);
			$this->yearRange_from = $settings["yearRange_from"] ? $settings["yearRange_from"] : 0;
			$this->yearRange_to = $settings["yearRange_to"] ? $settings["yearRange_to"] : 8;
			$this->isFixedYear = $settings["isFixedYear"] ? $settings["isFixedYear"] : false;
			$this->isSelectCurrentDate = $settings["isSelectCurrentDate"]===false ? false : true;
			$this->defaultYear = $settings["defaultYear"] ? $settings["defaultYear"] : false;
			$this->defaultMonth = $settings["defaultMonth"] ? $settings["defaultMonth"] : false;
			$this->defaultDay = $settings["defaultDay"] ? $settings["defaultDay"] : false;
		}
		public function yearOptions()
		{
			$from = 
				$this->isFixedYear ?
					$this->yearRange_from : 
					$this->currentYear-$this->yearRange_from;
			$to = 
				$this->isFixedYear ?
					$this->yearRange_to : 
					$this->currentYear+$this->yearRange_to;
			for($i=$from; $i<=$to; $i++)
			{
				$html = ((!$this->defaultYear && $this->isSelectCurrentDate && $this->currentYear==$i) || $this->defaultYear==$i) ?
					$html.= "<option value=\"".$i."\" selected=\"selected\">".$i."</option>\n" :
					$html.= "<option value=\"".$i."\">".$i."</option>\n";
			}
			echo $html;
		}
		public function monthOptions()
		{	
			for($i=1;$i<=12;$i++)
			{
				$html = ((!$this->defaultMonth && $this->isSelectCurrentDate && $this->currentMonth==$i) || $this->defaultMonth==$i) ?
					$html.= "<option value=\"".$i."\" selected=\"selected\">".$i."</option>\n" :
					$html.= "<option value=\"".$i."\">".$i."</option>\n";
			}
			echo $html;
		}
		public function dayOptions()
		{
			for($i=1;$i<=31;$i++)
			{
				$html = ((!$this->defaultDay && $this->isSelectCurrentDate && $this->currentDay==$i) || $this->defaultDay==$i) ?
					$html.= "<option value=\"".$i."\" selected=\"selected\">".$i."</option>\n" :
					$html.= "<option value=\"".$i."\">".$i."</option>\n";
			}
			echo $html;
		}
	}
	
?>