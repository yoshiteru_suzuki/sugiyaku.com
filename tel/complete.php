<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>電話・Skype相談予約 | 漢方相談 スギヤマ薬局</title>
<?php readfile("../head.html")?>
</head>
<body id="confirm" class="under">
<div id="container">
<?php readfile("../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>電話・Skype相談予約</span></p>
		<div id="mainContents">
			<p class="mb05">ご予約ありがとうございます。</p>
			<p class="mb05">すでに予約が入っている場合は、事前にご連絡いたします。</p>
			<p class="mb05">ご予約のお時間になりましたら、本店（042-746-1951）までお電話していただき、相談予約がある旨をお伝えくださいませ。</p>
			<p class="mb05">また、ご記入いただいたメールアドレスに自動返信メールを送信いたしました。もし数時間しても届かない場合はお手数ですが、再度送信していただくか、お電話にてご連絡ください。 </p>
		</div>
		<!--mainContents-->

		<div id="subContents">
<?php readfile("../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../footer.html")?>
</div>
</body>
</html>
