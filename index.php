<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns# fb: http://www.facebook.com/2008/fbml">

<head>
	<meta charset="utf-8">
	<meta name="description" content="座間市（小田急相模原駅）の漢方相談薬局です。" />
	<meta name="keywords" content="漢方,薬局,相談薬局,相模原,小田急相模原,座間,海老名,大和" />
	<meta property="og:title" content="漢方相談 スギヤマ薬局" />
	<meta property="og:description" content="座間市（小田急相模原駅）の漢方相談薬局です。" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://www.sugiyaku.com/" />
	<meta property="og:image" content="https://www.sugiyaku.com/common/images/sns_thumb.jpg" />
	<meta property="og:locale" content="ja_JP" />
	<title>漢方相談 スギヤマ薬局</title>
	<?php readfile("head.html") ?>
	<link href="/common/lib/jquery.neosmart.fb.wall/jquery.neosmart.fb.wall.css" rel="stylesheet" type="text/css" />
	<script src="/common/lib/jquery.neosmart.fb.wall/jquery.neosmart.fb.wall.js" type="text/javascript"></script>
	<script src="/common/js/home.js"></script>
</head>

<body id="home">
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.8&appId=296090683816521";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<h1 id="top_ttl">漢方相談のスギヤマ薬局</h1>

	<div id="container">
		<header id="siteHeader" class="inner">
			<div id="visual"></div>
			<nav class="clearfix">
				<ul id="gnavi">
					<li id="gnavi_info" class="gnavi-p">
						<a href="/info/">ごあんない</a>
						<ul class="gnavi-sub">
							<li><a href="/info/staff.html">スタッフ</a></li>
							<li><a href="/info/price.html">処方料金</a></li>
							<li><a href="/info/qa.html">Q&amp;A</a></li>
							<li><a href="/info/access.html">アクセス</a></li>
						</ul>
					</li>
					<li class="gnavi-p" id="gnavi_soudan"><a href="/soudan/">健康相談室</a></li>
					<li class="gnavi-p" id="gnavi_course"><a href="/course/">漢方・健康講座</a></li>
					<li class="gnavi-p" id="gnavi_takuya_b"><a href="/takuya_b/">タクヤ ブログ</a></li>
					<li class="gnavi-p snavi snavi-top"><a href="/yoyaku/">ご相談予約 総合受付</a></li>
					<li class="gnavi-p snavi"><a href="/tel/step.html">電話・スカイプ相談</a></li>
				</ul>
			</nav>
		</header>
		<p id="top_contact">
			<span class="mi">ご相談予約・お問い合わせ</span>
			<span class="tel">042−746−1951</span>
			<span class="open">9:00〜19:00</span>
			<span class="holiday">定休日：木・日曜・祝日</span>
		</p>
		<div class="bn-skype inner">
			<a href="/tel/step.html"><img src="/images/bn-skype.jpg" alt="日本全国どこからでも対面相談が可能です！"></a>
			<a href="/yoyaku/"><img src="/images/bn-yoyaku.jpg" alt="ご相談予約 総合受付"></a>
		</div>
		<div id="contents" class="inner clearfix">
			<div id="mainContents">
				<section class="sec clearfix topics">
					<div class="mi">ご相談予約のお願い</div>
					<div class="txt">
						<div class="whatsnew-entry">
							<p>おかげさまで皆様より大変多くのご相談を頂戴しており、現在予約が大変に混み合っている状況です。<br>
								お一人様にきちんとお時間を取り、ゆっくりとご相談いただけるようにしております関係上、ご相談は原則的に予約制とさせていただいております。<br>
								お手数ですが、ご相談希望の方はお電話（本店：042-746-1951）または<a href="/yoyaku/">ご相談予約総合受付</a>にて、相談日時のご予約にご協力をお願いいいたします。<br>
								※土曜日は特に予約が混み合いますので、できれば余裕をもって一週間以上前にご予約をしていただきますようお願いいたします。ご予約のないお客様の直接のご来局によるご相談はお断りすることもございます。大変申し訳ございませんが、どうかご了承くださいませ。</p>
						</div>
					</div>
				</section>
				<section class="sec clearfix topics">
					<div class="mi">お知らせ</div>
					<div class="txt">
						<?php readfile("topics/index.html") ?>
					</div>
				</section>
				<section id="blog" class="sec clearfix">
					Loading..
				</section>
				<section class="sec clearfix taikendan">
					<div class="mi">漢方改善体験談</div>
					<div class="txt">
						<div class="whatsnew-entry">
							<p>当店での漢方による改善体験談を「漢方体験.com」にて掲載中<br>現在投稿数はおかげさまで神奈川No1を独走中！<a href="http://kanpo-taiken.com/experience/result/4/388/" target="_blank">漢方体験.com</a></p>
						</div>
					</div>
				</section>
				<section id="facebook" class="sec clearfix">
					<div class="fb-like-box">
						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsugiyaku%2F&tabs=timeline&width=340&height=300&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId=125068230918821" width="340" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
					</div>
				</section>
				<section id="credit" class="sec">
					<ul>
						<!-- <li class="mb05">illustration: <a href="//piccopiccostoria.com/" target="_blank">Yukako Kobayashi piccopiccostoria.com</a></li> -->
						<li>ウェブ制作・管理：<a href="http://motif-xxx.com/" target="_blank">Yoshiteru Suzuki motif-xxx.com</a></li>
					</ul>
				</section>
				<section id="sns_btn" class="sec clearfix">
					<div class="tw">
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="takuyasensei" data-lang="ja">ツイート</a>
						<script>
							! function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (!d.getElementById(id)) {
									js = d.createElement(s);
									js.id = id;
									js.src = "//platform.twitter.com/widgets.js";
									fjs.parentNode.insertBefore(js, fjs);
								}
							}(document, "script", "twitter-wjs");
						</script>
					</div>
					<div class="fb-like" data-href="http://www.sugiyaku.com/" data-send="false" data-layout="button_count" data-width="200" data-show-faces="true"></div>
				</section>
			</div>
			<!--mainContents-->

			<div id="subContents">
				<?php readfile("subContents.html") ?>
			</div>
			<!--subContents-->

		</div>
		<!--contents-->


		<?php readfile("footer.html") ?>
	</div>
</body>

</html>