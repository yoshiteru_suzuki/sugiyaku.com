<?php

// デバックモード（1:ローカルデバック、2:サーバデバック、0:本番）

$pattern = "/127\.0\.0\.1|192\.168\.10\.\d|10\.0\.1\.\d/";
$str = $_SERVER["HTTP_HOST"];
if (preg_match($pattern, $str, $match)) {
	define("DEBUG", 1);
	$to_adrress = "fiatfirepanda@gmail.com";
} else if (preg_match("/test\.sugiyaku\.com/", $str, $match)) {
	define("DEBUG", 2);
	$to_adrress = 'info@sugiyaku.com,takuya0613@gmail.com';
} else {
	define("DEBUG", 0);
	$to_adrress = 'info@sugiyaku.com,masumi.k-ph@sugiyaku.com,takuya0613@gmail.com';
}

ini_set("mbstring.internal_encoding", "UTF-8");
ini_set("mbstring.http_output", "UTF-8");
ini_set("mbstring.encoding_translation", "Off");
ini_set("session.use_trans_sid", 0);
mb_http_output('UTF-8');
mb_language("Japanese");
mb_internal_encoding("UTF-8");
