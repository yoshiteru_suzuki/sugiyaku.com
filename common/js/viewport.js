/*
 * デバイス処理
 */
var spView, tbView;
function setViewport(){
	spView = 'width=device-width,initial-scale=1';
	tbView = 'width=920,initial-scale=1';
	var ua = navigator.userAgent;
	if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {
		$('head').prepend('<meta name="viewport" content="' + spView + '" id="viewport">');
	} else {
		$('head').prepend('<meta name="viewport" content="' + tbView + '" id="viewport">');
	}
}
setViewport();
