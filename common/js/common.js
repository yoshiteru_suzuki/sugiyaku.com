var smart=false;
if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('Android') > 0) {
	smart=true;
}

var ie = (function(){
	var undef, v = 3, div = document.createElement('div');
	while (div.innerHTML = '<!--[if gt IE '+(++v)+']><i></i><![endif]-->',div.getElementsByTagName('i')[0]);
	return v> 4 ? v : undef;
}());


$().ready(function(){
	smartRollover();
	if(!ie || ie>=9)
	{
		$("#gnavi a").mouseover(function(){
			$(this).stop(true, true);
			$(this).animate({backgroundPosition:"(0px 0px)"}, 200, 'linear');
		});
		$("#gnavi a").mouseout(function(){
			$(this).stop(true, true);
			$(this).animate({backgroundPosition:"(0px 10px)"}, 100, 'linear');
		});
	}
	var pathn = location.pathname;
	var kaisou = pathn.split("/");
	if(kaisou[1] != "")
	{
		var gid = "#gnavi_"+kaisou[1];
		$(gid).append('<img src="/common/images/gnavi_arrow.gif" alt="↓" id="gnavi_arrow" />');
	}
	$('a[href^="#"]').smoothScroll();

	// scroll
	$(window).scroll(function () {
		if($(window).scrollTop() > 0) $('#ft_pagetop').fadeIn(500);
		else  $('#ft_pagetop').fadeOut(200);
	});

	// smart phone
	if(smart)
	{
		$("body").append('<div id="menu">MENU</div>');
		$("#menu").click(function () {
	  	if($("#menu").text()=="MENU"){
				$("#menu").text("CLOSE");
				$("#gnavi").addClass('open');
			}else{
				$("#menu").text("MENU");
				$("#gnavi").removeClass('open');
			}
		});
		// $("#menu").click(function () {
		//   $("#gnavi").slideToggle(500,function(){
		//   	if($("#menu").text()=="MENU")
		//   		$("#menu").text("CLOSE");
		//   	else
		//   		$("#menu").text("MENU");
		//   });
		// });
	}
	getRanking();
});

function getRanking(){
	$.get("/php/ranking/ranking.php?view=1", setRanking);
}
function setRanking(myData, myStatus) {
	$('#side_ranking').append(myData);
}

function smartRollover() {

	if( $.browser.msie && $.browser.version<7)
	{
		$("img, input").each(function()
		{
			if($(this).attr("src").match("_default.png"))
			{
				$(this)
				.data("src",$(this).attr("src"))
				.attr("src","/common/images/blank.gif")
				.css("filter","progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+$(this).data("src")+"',sizingMethod='scale')");
				$('<img/>').attr( 'src', $(this).data("src").replace("_default.", "_over.") );
				$(this).mouseover(
					function() {
						$(this).css("filter","progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+$(this).data("src").replace("_default.", "_over.")+"',sizingMethod='scale')")
					}
				);
				$(this).mouseout(
					function() {
						$(this).css("filter","progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+$(this).data("src")+"',sizingMethod='scale')")
					}
				);
			}
			else if($(this).attr("src").match("_default."))
			{
				$('<img/>').attr( 'src', $(this).attr("src").replace("_default.", "_over.") );

				$(this).mouseover(
					function() {
						$(this).attr("src", $(this).attr("src").replace("_default.", "_over."));
					}
				);
				$(this).mouseout(
					function() {
						$(this).attr("src", $(this).attr("src").replace("_over.", "_default."));
					}
				);
			}
			else if($(this).attr("src").match(".png"))
			{
				$(this)
				.data("src",$(this).attr("src"))
				.attr("src","/common/images/blank.gif")
				.css("filter","progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+$(this).data("src")+"',sizingMethod='scale')");
			}
		});
	}
	else
	{
		$("img, input").each(function (){
			if($(this).attr("src"))
			{
				if($(this).attr("src").match("_default."))
				{
					$('<img/>').attr( 'src', $(this).attr("src").replace("_default.", "_over.") );

					$(this).mouseover(
						function() {
							$(this).attr("src", $(this).attr("src").replace("_default.", "_over."));
						}
					);
					$(this).mouseout(
						function() {
							$(this).attr("src", $(this).attr("src").replace("_over.", "_default."));
						}
					);
				}
			}
		});
	}
}


/**
 * Flatten height same as the highest element for each row.
 *
 * Copyright (c) 2011 Hayato Takenaka
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * @author: Hayato Takenaka (http://urin.take-uma.net)
 * @version: 0.0.2
**/
;(function($) {
	$.fn.tile = function(columns) {
		var tiles, max, c, h, last = this.length - 1, s;
		if(!columns) columns = this.length;
		this.each(function() {
			s = this.style;
			if(s.removeProperty) s.removeProperty("height");
			if(s.removeAttribute) s.removeAttribute("height");
		});
		return this.each(function(i) {
			c = i % columns;
			if(c == 0) tiles = [];
			tiles[c] = $(this);
			h = tiles[c].height();
			if(c == 0 || h > max) max = h;
			if(i == last || c == columns - 1)
				$.each(tiles, function() { this.height(max); });
		});
	};
})(jQuery);


/**
* @author Alexander Farkas
* v. 1.02
*/
(function($) {
$.extend($.fx.step,{
backgroundPosition: function(fx) {
if (fx.state === 0 && typeof fx.end == 'string') {
var start = $.curCSS(fx.elem,'backgroundPosition');
start = toArray(start);
fx.start = [start[0],start[2]];
var end = toArray(fx.end);
fx.end = [end[0],end[2]];
fx.unit = [end[1],end[3]];
}
var nowPosX = [];
nowPosX[0] = ((fx.end[0] - fx.start[0]) * fx.pos) + fx.start[0] + fx.unit[0];
nowPosX[1] = ((fx.end[1] - fx.start[1]) * fx.pos) + fx.start[1] + fx.unit[1];
fx.elem.style.backgroundPosition = nowPosX[0]+' '+nowPosX[1];

function toArray(strg){
strg = strg.replace(/left|top/g,'0px');
strg = strg.replace(/right|bottom/g,'100%');
strg = strg.replace(/([0-9\.]+)(\s|\)|$)/g,"$1px$2");
var res = strg.match(/(-?[0-9\.]+)(px|\%|em|pt)\s(-?[0-9\.]+)(px|\%|em|pt)/);
return [parseFloat(res[1],10),res[2],parseFloat(res[3],10),res[4]];
}
}
});
})(jQuery);
