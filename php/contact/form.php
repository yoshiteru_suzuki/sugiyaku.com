<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>お問い合わせ | 漢方相談 スギヤマ薬局</title>
<?php readfile("../../head.html")?>
<link rel="stylesheet" href="/common/lib/jquery.validationEngine/validationEngine.jquery.css" type="text/css">
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine-ja.js" type="text/javascript"></script>
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
<script>
$(function() {
	$("#form-contact").validationEngine();
});
</script>
</head>
<body id="form" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>お問い合わせ</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">お問い合わせ</h1>
			<div class="box1 mb25">
				<p>健康・漢方薬・お身体に関するご相談・お問い合わせは<a href="/yoyaku/">総合受付</a>からご予約ください。</p>
			</div>
			<p class="mb20">下記のフォームにご記入の上送信してください。</p>

			<form action="index.php" method="post" id="form-contact">

				<table class="formTbl01">
				<tr>
				<th>お名前<b class="hiss">*</b></th>
				<td><input type="text" name="name" value="<?php echo $DISP["name"] ?>" class="txtbox02 validate[required]" title="お名前を入力してください"  /></td></tr>
				<tr><th>フリガナ<b class="hiss">*</b></th>
				<td><input type="text" name="name_kana" value="<?php echo $DISP["name_kana"] ?>" class="txtbox02 validate[required]" title="フリガナを入力してください" maxlength="20"  /></td>
				</tr>
				<tr>
				<th>郵便番号</th>
				<td>〒 <input type="text" name="zip" value="<?php echo $DISP["zip"] ?>" class="txtbox03" title="郵便番号を入力してください" maxlength="8"  /></td>
				</tr>
				<tr>
				<th>ご住所</th>
				<td><input type="text" name="adr" value="<?php echo $DISP["adr"] ?>" class="txtbox01" title="ご住所を入力してください"  maxlength="100"  /></td>
				</tr>
				<tr>
				<th>電話番号</th>
				<td><input type="text" name="tel1" value="<?php echo $DISP["tel1"] ?>" class="txtbox01" title="電話番号を入力してください"  maxlength="20"  /></td>
				</tr>
				<tr>
				<th>E-Mail<b class="hiss">*</b></th>
				<td><input type="text" name="e_mail" value="<?php echo $DISP["e_mail"] ?>" class="txtbox01 validate[required, custom[email]]" title="E-Mailを正しく入力してください"  maxlength="100"  /></td>
				</tr>
				<tr>
				<th>お問い合わせ内容<b class="hiss">*</b></th>
				<td><textarea name="remarks" rows="10" class="validate[required]"><?php echo $DISP["remarks"] ?></textarea></td>
				</tr>
				<tr>
				</tr>
				</table>

				<input type="hidden" name="action" value="confirm" />
				<p class="mb10 txtRight"><a href="/php/policy.html">個人情報の取り扱い &raquo;</a></p>
				<div class="btn_submit"><input type="submit" name="submit" value="確認画面へ進む" class="input_b"   /></div>
			</form>
		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
</body>
</html>
