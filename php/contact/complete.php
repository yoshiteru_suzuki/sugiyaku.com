<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>お問い合わせ | 漢方相談 スギヤマ薬局</title>
<?php readfile("../../head.html")?>
</head>
<body id="form" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>お問い合わせ</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">お問い合わせ</h1>
			<p class="mb05">お問い合わせありがとうございます。</p>
			<p class="mb05">ご記入いただいたメールアドレスに自動返信メールを送信いたしました。<br />もし数時間しても届かない場合はお手数ですが、再度送信していただくか、お電話にてご連絡ください。 </p>
		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
</body>
</html>
