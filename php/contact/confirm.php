<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>お問い合わせ | 漢方相談 スギヤマ薬局</title>
<?php readfile("../../head.html")?>
<link rel="stylesheet" href="/common/lib/jquery.validationEngine/validationEngine.jquery.css" type="text/css">
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine-ja.js" type="text/javascript"></script>
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
</head>
<body id="form" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>お問い合わせ</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">お問い合わせ</h1>

			<p class="mb10">入力内容をご確認の上送信してください。</p>

			<form action="index.php" method="post" id="form-contact">

				<input type="hidden" name="name" value="<?php echo $DISP["name"]?>" />
				<input type="hidden" name="name_kana" value="<?php echo $DISP["name_kana"]?>" />
				<input type="hidden" name="zip" value="<?php echo $DISP["zip"]?>" />
				<input type="hidden" name="adr" value="<?php echo $DISP["adr"]?>" />
				<input type="hidden" name="tel1" value="<?php echo $DISP["tel1"]?>" />
				<input type="hidden" name="e_mail" value="<?php echo $DISP["e_mail"]?>" />
				<input type="hidden" name="remarks" value="<?php echo $DISP["remarks"]?>" />

				<table class="formTbl01">
				<tr>
				<th>お名前</th>
				<td><?php if( $DISP["name"] ){ echo $DISP["name"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>フリガナ</th>
				<td><?php if( $DISP["name_kana"] ){ echo $DISP["name_kana"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>郵便番号</th>
				<td>〒 <?php if( $DISP["zip"] ){ echo $DISP["zip"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>ご住所</th>
				<td><?php if( $DISP["adr"] ){ echo $DISP["adr"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>電話番号</th>
				<td><?php if( $DISP["tel1"] ){ echo $DISP["tel1"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>E-Mail</th>
				<td><?php if( $DISP["e_mail"] ){ echo $DISP["e_mail"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>お問い合わせ内容</th>
				<td><?php if( $DISP["remarks"] ){ echo nl2br($DISP["remarks"]); }else{ echo "&nbsp"; } ?></textarea></td>
				</tr>
				<tr>
				</tr>
				</table>
				<input type="hidden" name="action" value="complete" />

				<div class="btn_submit">
					<input type="submit" name="submit[back]" value="入力内容を修正" class="input_b"   />
					<input type="submit" name="submit[done]" value="上記の内容で送信" class="input_b"   />
				</div>
			</form>
		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
</body>
</html>
