<?

	if($_POST['submit'])
	{
		extract($_POST);

		//テキストフィールドを取得
		$txtdata = array(
												'1. お名前'		=> @$name,
												'2. E-Mail'		=> @$email,
												'3. お住まい'	=> @$adrken,
												'4. 性別'	=> @$sex,
												'5. 生年月日'	=> @$birth_y."年".@$birth_m."月".@$birth_d."日　".@$age."歳",
												'6. 今一番気になる症状は？'	=> @$shoujyou1,
												'7. いつ頃からですか？原因は？'	=> @$shoujyou2,
												'8. その症状が酷くなるのは？（例、朝起きた時、疲れたとき、冬など）'	=> @$shoujyou3,
												'9. 今までに病気をしたことがありますか？いつ頃？'	=> @$shoujyou4,
												'10. 体格は？'	=> @$taichou1."cm ".@$taichou2."kg",
												'11. 顔色は？'	=> GetCheckData($_POST['taichou3']),
												'12. 寒熱は？'	=> GetCheckData($_POST['taichou4']),
												'13. 汗は？'	=> GetCheckData($_POST['taichou5']),
												'14. 睡眠は？'	=> GetCheckData($_POST['taichou6']),
												'15. 食欲は？'	=> GetCheckData($_POST['taichou7']),
												'16. 飲物は？'	=> GetCheckData($_POST['taichou8']),
												'17. 便通は？'	=> GetCheckData($_POST['taichou9']),
												'18. 小便は？'	=> GetCheckData($_POST['taichou10']),
												'19. 体質は？'	=> GetCheckData($_POST['taichou11']),
												'20. 血圧は？'	=> "上：".@$taichou12."下：".@$taichou13." ".@$taichou14,
												'21.現在飲んでる漢方薬:  '	=> @$taichou15,
												'22-1：舌の色は？（表面に苔がついている場合は周辺の色を見て下さい）'	=> GetCheckData($_POST['bero1']),
												'22-2：苔は？（どちらかに当てはまる場合のみチェックしてください。白い苔が薄くついているのは正常です）'	=> GetCheckData($_POST['bero2']),
												'22-3：下記の舌に当てはまる場合はチェックしてください。'	=> GetCheckData($_POST['bero3']),
												'23. 出産経験はありますか？'	=> GetCheckData($_POST['women1']),
												'24. 生理の周期は？'	=> GetCheckData($_POST['women2']),
												'25. 生理の出血は？'	=> GetCheckData($_POST['women3']),
												'26. 生理の期間は？'	=> $_POST['women4']."日",
												'27. 生理痛は？'	=> GetCheckData($_POST['women5']),
												'28. 経血の状態は？'	=> GetCheckData($_POST['women6']),
												'29. 生理前後の状態は？'	=> GetCheckData($_POST['women7'])
											);


		// パラメータ

		// メール送信
		$from_adrress = 'admin@sugiyaku.com';
		$to_adrress_user = @$email;
//		$to_adrress = 'info@sugiyaku.com,takuya0613@gmail.com';
		$to_adrress = 'ys@neothink.ne.jp';
		$reply = 'info@sugiyaku.com';
		$subject = "ご相談受付 タクヤ先生の相談室 「".@$page."」";

		// ファイル出力
		$today = date("YmdHis");
		$filename = "./soudandata/soudandata".$today."txt";

		//日本語をしますよ
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");

		$mail_body = @$name."様\n\n";
		$mail_body .= "ご相談、ありがとうございます。\n";
		$mail_body .= "お送りいただきました相談フォームをもとにお返事をさせていただきます。\n";
		$mail_body .= "こちらからの返信が届くまで今しばらくお待ちくださいませ。\n";
		$mail_body .= "\n";
		$mail_body .= "万一、2日以上ご返信が無い場合はお手数ですがお電話、\nもしくはメール(info@sugiyaku.com)にてお問い合わせください。\nよろしくお願いいたします。\n";
		$mail_body .= "\n";
		$mail_body .= "\n";
		$mail_body .= "============================================================================\n";
		$mail_body .= "漢方の杉山薬局\n";
		$mail_body .= "住所　　：神奈川県座間市相模が丘5-10-37\n";
		$mail_body .= "電話　　：042-746-1951\n";
		$mail_body .= "営業時間：午前9時～午後19時\n";
		$mail_body .= "定休日　：日曜日、祝祭日\n";
		$mail_body .= "E-mail　：info@sugiyaku.com\n";
		$mail_body .= "URL 　　：http://www.sugiyaku.com/\n";
		$mail_body .= "============================================================================\n";


		//相談した人へ
		SendToMail($to_adrress_user, $reply, $subject, $mail_body, $from_adrress, "");


		//杉山薬局へ

		//本文
		$mail_body = "";
		while(list ($key, $val) = each($txtdata)) {
			$mail_body .= "-------------\n$key\n$val\n\n";
			//print "$key, $val<br />";
		}
		SendToMail($to_adrress, $reply, $subject, $mail_body, $from_adrress, "");

		// ファイルへ出力
		MakeFile($filename, $mail_body);


		//complete.phpページへ飛びます。
		header("Location: http://www.sugiyaku.com/07/php/soudan_complete.php");
		exit;

	}

	//メール送信
	function SendToMail( $to, $reply, $subject, $body, $from, $opt )
	{
		$head = "From: ".$from."\n";
		$head .= "Reply-To: ".$reply."\n";
		$head .= "Mime-Version: 1.0\n";
		$head .= "Content-Type: text/plain; charset=ISO-2022-JP\n";
		$head .= "Content-Transfer-Encoding: 7bit\n";
		$opt = $opt;
		mail(
		    $to,
		    mb_encode_mimeheader(mb_convert_encoding($subject, "JIS"), "ISO-2022-JP", "B"),
		    mb_convert_encoding($body, "JIS"),
		    $head,
		    $opt
		);
	}

	//チェックボックス選択アイテム取得
	function GetCheckData($itemname)
	{

		$ret = "";

		for($i = 0; $i < count(@$itemname); $i++)
		{
		    if($i != 0){ $ret .= "・"; }
		    $ret .= $itemname[$i];
		}

		return $ret;

	}

	// ファイル書き込み
	function MakeFile($filename, $data)
	{

		$filepointer = fopen( $filename, 'w');
		flock($filepointer, LOCK_EX);
		fwrite( $filepointer, $data);
		flock($filepointer, LOCK_UN);
		fclose( $filepointer );

	}



//ドキュメントルート ディレクトリからinclude_pathを追加する
$path = $_SERVER["DOCUMENT_ROOT"]."/07/ssi";
//include_pathに新しいパスを追加
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
$title = "相談フォーム";

$pagetype = array('健康になりたい', '漢方美容', '不妊相談', 'その他');

$page = $pagetype[$_GET['page']];


include 'header.php';

?>

<body id="soudanform" onload="init('soudan_form');">

	<a name="top" id="top"></a>

	<div id="wrapper">

		<!-- ↓ hea -->
<?
include 'hea.html';
?>
		<!-- ↑ hea -->

		<!-- ↓ main -->
		<div id="main">

			<!-- ↓ main box-->
			<div class="box-top">
				<div class="box-bottom">
					<div class="box-line">

						<!-- ↓ fbox-->
						<div class="fbox">

							<!-- 左ナビゲーション部分領域の作成用 -->
							<div id="leftblank"></div>

							<!-- ↓ contents -->
							<div id="contents">

								<!-- ↓ contents-inner -->
								<div id="contents-inner">

									<form name="form-soudanform" id="form-soudanform" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

										<input type="hidden" name="page" value="<?=$page?>" />

										<div class="soudanformlist">

											<div id="soudanform-kihon">

												<div class="fbox">
													<div class="ft"><label for="">1. お名前</label></div>
													<div class="fd"><input type="text"  name="name" id="name" class="required" title="お名前を入力してください" /></div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">2. E-Mail</label></div>
													<div class="fd"><input type="text" name="email" id="email" class="required validate-email" title="E-Mailを正しく入力してください" /></div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">3. お住まい</label></div>
													<div class="fd">
														<select name="adrken" id="adrken">
														<option value="北海道">北海道 </option>
														<option value="青森">青森 </option>
														<option value="岩手">岩手 </option>
														<option value="宮城">宮城 </option>
														<option value="秋田">秋田 </option>
														<option value="山形">山形 </option>
														<option value="福島">福島 </option>
														<option value="茨城">茨城 </option>
														<option value="栃木">栃木 </option>
														<option value="群馬">群馬 </option>
														<option value="埼玉">埼玉 </option>
														<option value="千葉">千葉 </option>
														<option value="東京">東京 </option>
														<option value="神奈川">神奈川 </option>
														<option value="新潟">新潟 </option>
														<option value="富山">富山 </option>
														<option value="石川">石川 </option>
														<option value="福井">福井 </option>
														<option value="山梨">山梨 </option>
														<option value="長野">長野 </option>
														<option value="岐阜">岐阜 </option>
														<option value="静岡">静岡 </option>
														<option value="愛知">愛知 </option>
														<option value="三重">三重 </option>
														<option value="滋賀">滋賀 </option>
														<option value="京都">京都 </option>
														<option value="大阪">大阪 </option>
														<option value="兵庫">兵庫 </option>
														<option value="奈良">奈良 </option>
														<option value="和歌山">和歌山 </option>
														<option value="鳥取">鳥取 </option>
														<option value="島根">島根 </option>
														<option value="岡山">岡山 </option>
														<option value="広島">広島 </option>
														<option value="山口">山口 </option>
														<option value="徳島">徳島 </option>
														<option value="香川">香川 </option>
														<option value="愛媛">愛媛 </option>
														<option value="高知">高知 </option>
														<option value="福岡">福岡 </option>
														<option value="佐賀">佐賀 </option>
														<option value="長崎">長崎 </option>
														<option value="熊本">熊本 </option>
														<option value="大分">大分 </option>
														<option value="宮崎">宮崎 </option>
														<option value="鹿児島">鹿児島 </option>
														<option value="沖縄">沖縄 </option>
														<option value="海外" selected="selected">海外</option>
														</select>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">4. 性別</label></div>
													<div class="fd">
														<select name="sex" id="sex">
														<option value="男性">男性</option>
														<option value="女性" selected="selected">女性</option>
														</select></td>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">5. 生年月日</label></div>
													<div class="fd">
														<input type="text" name="birth_y" id="birth_y" value="19" maxlength="4" class="validate-number" title="入力内容が正しくありません"  />年　　
														<select name="birth_m" id="birth_m">
														<option selected="selected">1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
														<option>6</option>
														<option>7</option>
														<option>8</option>
														<option>9</option>
														<option>10</option>
														<option>11</option>
														<option>12</option>
														</select>
														月
														<select name="birth_d" id="birth_d">
														<option selected="selected">1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
														<option>6</option>
														<option>7</option>
														<option>8</option>
														<option>9</option>
														<option>10</option>
														<option>11</option>
														<option>12</option>
														<option>13</option>
														<option>14</option>
														<option>15</option>
														<option>16</option>
														<option>17</option>
														<option>18</option>
														<option>19</option>
														<option>20</option>
														<option>21</option>
														<option>22</option>
														<option>23</option>
														<option>24</option>
														<option>25</option>
														<option>26</option>
														<option>28</option>
														<option>29</option>
														<option>29</option>
														<option>30</option>
														<option>31</option>
														</select>

														日
														<input type="text" name="age" id="age" size="5" maxlength="3" class="validate-number" title="入力内容が正しくありません" />
														才 
													</div>
												</div>

											</div>

										</div>

										<div class="soudanformlist">

											<div id="soudanform-shoujyou">

												<div class="ft"><label for="">6. 今一番気になる症状は？</label></div>
												<div class="fd"><textarea name="shoujyou1" id="shoujyou1" class=""></textarea></div>

												<div class="ft"><label for="">7. いつ頃からですか？原因は？</label></div>
												<div class="fd"><textarea  name="shoujyou2" id="shoujyou2" class=""></textarea></div>

												<div class="ft"><label for="">8. その症状が酷くなるのは？（例、朝起きた時、疲れたとき、冬など）</label></div>
												<div class="fd"><textarea name="shoujyou3" id="shoujyou3" class=""></textarea></div>

												<div class="ft"><label for="">9. 今までに病気をしたことがありますか？いつ頃？</label></div>
												<div class="fd"><textarea name="shoujyou4" id="shoujyou4" class=""></textarea></div>

											</div>

										</div>

										<div class="soudanformlist">

											<div id="soudanform-taichou">

												<h3>あなたの現在の体調にあてはまるものをチェックして下さい。</h3>

												<div class="fbox">
													<div class="ft"><label for="">10. 体格は？</label></div>
													<div class="fd">
														<input type="text" name="taichou1" id="taichou1" size="5" maxlength="3" />（センチ）　
														体重<input type="text" name="taichou2" id="taichou2" size="5" maxlength="3" />（キロ）
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">11. 顔色は？</label></div>
													<div class="fd">
														<label><input name="taichou3[]" type="checkbox" value="血色が悪く白っぽい" />血色が悪く白っぽい</label>
														<label><input name="taichou3[]" type="checkbox" value="のぼせがちで赤くほてっている" />のぼせがちで赤くほてっている</label>
														<label><input name="taichou3[]" type="checkbox" value="血行が悪くくすんでいる" />血行が悪くくすんでいる</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">12. 寒熱は？</label></div>
													<div class="fd">
														<div class="fbox">
															<label class="f180px"><input name="taichou4[]" type="checkbox" value="顔や手足がほてる" />顔や手足がほてる</label>
															<label class="f180px"><input name="taichou4[]" type="checkbox" value="手足の先のみ冷える" />手足の先のみ冷える</label>
														</div>
														<div class="fbox">
															<label class="f180px"><input name="taichou4[]" type="checkbox" value="顔がのぼせて足が冷える" />顔がのぼせて足が冷える</label>
															<label class="f180px"><input name="taichou4[]" type="checkbox" value="全身が冷える" />全身が冷える</label>
														</div>
														<div class="fbox">
															<label class="f180px"><input name="taichou4[]" type="checkbox" value="どちらかというと冷える" />どちらかというと冷える</label>
															<label class="f180px"><input name="taichou4[]" type="checkbox" value="下半身が冷える" />下半身が冷える</label>
														</div>
														<label><input name="taichou4[]" type="checkbox" value="どちらかというと暑がり" />どちらかというと暑がり</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">13. 汗は？</label></div>
													<div class="fd">
														<div class="fbox">
															<label class="f180px"><input name="taichou5[]" type="checkbox" value="汗が多い" />汗が多い</label>
															<label class="f180px"><input name="taichou5[]" type="checkbox" value="寝汗をよくかく" />寝汗をよくかく</label>
														</div>
														<div class="fbox">
															<label class="f180px"><input name="taichou5[]" type="checkbox" value="動くとすぐに汗をかく" />動くとすぐに汗をかく</label>
															<label class="f180px"><input name="taichou5[]" type="checkbox" value="季節を問わず汗かきだ" />季節を問わず汗かきだ</label>
														</div>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">14. 睡眠は？</label></div>
													<div class="fd">
														<div class="fbox">
															<label class="f150px"><input name="taichou6[]" type="checkbox" value="寝付きが悪い" />寝付きが悪い</label>
															<label><input name="taichou6[]" type="checkbox" value="眠りが浅いor夜中に目が醒める" />眠りが浅いor夜中に目が醒める</label>
														</div>
														<div class="fbox">
															<label class="f150px"><input name="taichou6[]" type="checkbox" value="日中いつも眠い" />日中いつも眠い</label>
															<label><input name="taichou6[]" type="checkbox" value="食後すぐ眠くなる" />食後すぐ眠くなる</label>
														</div>
														<label><input name="taichou6[]" type="checkbox" value="朝起きられない" />朝起きられない</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">15. 食欲は？</label></div>
													<div class="fd">
														<div class="fbox">
															<label class="f150px"><input name="taichou7[]" type="checkbox" value="あまり食欲がない" />あまり食欲がない</label>
															<label><input name="taichou7[]" type="checkbox" value="食べると胃がもたれる" />食べると胃がもたれる</label>
														</div>
														<div class="fbox">
															<label class="f150px"><input type="checkbox" value="食べてもすぐ腹が減る" />食べてもすぐ腹が減る</label>
															<label><input name="taichou7[]" type="checkbox" value="食欲はあるがあまり食べられない" />食欲はあるがあまり食べられない</label>
														</div>
														<label><input name="taichou7[]" type="checkbox" value="よく胃痛がする" />よく胃痛がする</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">16. 飲物は？</label></div>
													<div class="fd">
														<div class="fbox">
															<label class="f230px"><input name="taichou8[]" type="checkbox" value="口やのどが渇いてよく水分をとる" />口やのどが渇いてよく水分をとる</label>
															<label ><input name="taichou8[]" type="checkbox" value="あまり水分を摂らない" />あまり水分を摂らない</label>
														</div>
														<div class="fbox">
															<label class="f230px"><input name="taichou8[]" type="checkbox" value="冷たい飲物が好き" />冷たい飲物が好き</label>
															<label ><input name="taichou8[]" type="checkbox" value="暖かい飲み物が好き" />暖かい飲み物が好き</label>
														</div>
														<label><input name="taichou8[]" type="checkbox" value="水分をとるように心がけている" />水分をとるように心がけている</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">17. 便通は？</label></div>
													<div class="fd">
														<div class="fbox">
															<label class="f180px"><input name="taichou9[]" type="checkbox" value="便秘で薬を飲んでいる" />便秘で薬を飲んでいる</label>
															<label class="f180px"><input name="taichou9[]" type="checkbox" value="よく下痢をする" />よく下痢をする</label>
														</div>
														<div class="fbox">
															<label class="f180px"><input name="taichou9[]" type="checkbox" value="腹が張ってよくガスが出る" />腹が張ってよくガスが出る</label>
															<label class="f180px"><input name="taichou9[]" type="checkbox" value="便秘気味だ" />便秘気味だ</label>
														</div>
														<label class="f180px"><input name="taichou9[]" type="checkbox" value="軟便気味だ" />軟便気味だ</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">18. 小便は？</label></div>
													<div class="fd">
														<div class="fbox">
															<label class="f180px"><input name="taichou10[]" type="checkbox" value="トイレが近い（日中）" />トイレが近い（日中）</label>
															<label class="f180px"><input name="taichou10[]" type="checkbox" value="トイレが近い（夜間）" />トイレが近い（夜間）</label>
														</div>
														<div class="fbox">
															<label class="f180px"><input name="taichou10[]" type="checkbox" value="色が透明だ" />色が透明だ</label>
															<label class="f180px"><input name="taichou10[]" type="checkbox" value="色が濃い" />色が濃い</label>
														</div>
														<label class="f180px"><input name="taichou10[]" type="checkbox" value="あまりトイレに行かない" />あまりトイレに行かない</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">19. 体質は？</label></div>
													<div class="fd">
														<div class="fbox">
															<label class="f160px"><input name="taichou11[]" type="checkbox" value="疲れやすい" />疲れやすい</label>
															<label class="f210px"><input name="taichou11[]" type="checkbox" value="風邪をひきやすい" />風邪をひきやすい</label>
														</div>
														<div class="fbox">
															<label class="f160px"><input name="taichou11[]" type="checkbox" value="貧血気味だ" />貧血気味だ</label>
															<label class="f210px"><input name="taichou11[]" type="checkbox" value="上半身（顔など）がむくみやすい" />上半身（顔など）がむくみやすい</label>
														</div>
														<div class="fbox">
															<label class="f160px"><input name="taichou11[]" type="checkbox" value="肩こりor頭痛がする" />肩こりor頭痛がする</label>
															<label class="f210px"><input name="taichou11[]" type="checkbox" value="下半身（足など）がむくみやすい" />下半身（足など）がむくみやすい</label>
														</div>
														<div class="fbox">
															<label class="f160px"><input name="taichou11[]" type="checkbox" value="神経質だ" />神経質だ</label>
															<label class="f210px"><input name="taichou11[]" type="checkbox" value="イライラしやすい" />イライラしやすい</label>
														</div>
														<div class="fbox">
															<label class="f160px"><input name="taichou11[]" type="checkbox" value="腰や膝が痛いorだるい" />腰や膝が痛いorだるい</label>
															<label class="f210px"><input name="taichou11[]" type="checkbox" value="肌が荒れやすい" />肌が荒れやすい</label>
														</div>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">20. 血圧は？</label></div>
													<div class="fd">
														上が（<input name="taichou12" type="text" id="64. 血圧は？" size="5" maxlength="3" />㎜Hg）
														下が（<input name="taichou13" type="text" id="65. 血圧は？" size="5" maxlength="3" />㎜Hg）
														<label><input name="taichou14" type="checkbox" value="降圧剤を服用している" />降圧剤を服用している</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">21..現在飲んでる漢方薬:  </label></div>
													<div class="fd"><textarea name="taichou15" class=""></textarea></div>
												</div>

											</div>

										</div>
										<!-- ↑  -->

										<!-- ↓  -->
										<div class="soudanformlist">

											<div id="soudanform-bero">

												<h3>舌の状態は？</h3>
												<p>
													あなたの状態に近いものを選んでください。<br />
													｢正常な舌｣の写真を参考にあてはまるものをすべてチェックしてください。<br />
												</p>
												<p>
													※下の写真をクリックすると拡大してはっきり見えます。
												</p>

												<div class="ft"><label for="">22-1：舌の色は？（表面に苔がついている場合は周辺の色を見て下さい）</label></div>
												<div class="fd">

													<div class="fbox">
														<div class="bero22-1">
															<div class="berodesc">薄く赤みがかかり、鮮やかなピンク色</div>
															<div class="beropict">
																<a href="/07/img/bero/zoom/1.jpg" rel="lightbox" title="薄く赤みがかかり、鮮やかなピンク色"><img src="/07/img/bero/1.jpg" alt="" border="0" /></a></a>
															</div>
															<div class="berocheck">
																<label><input name="bero1[]" type="checkbox" value="正常な舌" />正常な舌</label>
															</div>
														</div>

														<div class="bero22-1">
															<div class="berodesc">白っぽいピンク色</div>
															<div class="beropict">
																<a href="/07/img/bero/zoom/2.jpg" rel="lightbox" title="白っぽいピンク色"><img src="/07/img/bero/2.jpg" alt="" border="0" /></a>
															</div>
															<div class="berocheck">
																<label><input name="bero1[]" type="checkbox" value="淡白舌" />淡白舌</label>
															</div>
														</div>

														<div class="bero22-1">
															<div class="berodesc">濃い赤色</div>
															<div class="beropict">
																<a href="/07/img/bero/zoom/3.jpg" rel="lightbox" title="濃い赤色"><img src="/07/img/bero/3.jpg" alt="" border="0" /></a>
															</div>
															<div class="berocheck">
																<label><input name="bero1[]" type="checkbox" value="紅舌" />紅舌</label>
															</div>
														</div>

														<div class="bero22-1">
															<div class="berodesc">紫がかっているor紫の小さな斑点がある</div>
															<div class="beropict">
																<a href="/07/img/bero/zoom/4.jpg" rel="lightbox" title="紫舌"><img src="/07/img/bero/4.jpg" alt="" border="0" /></a>
															</div>
															<div class="berocheck">
																<label><input name="bero1[]" type="checkbox" value="紫舌" />紫舌</label>
															</div>
														</div>
													</div>

												</div>

												<div class="ft"><label for="">22-2：苔は？（どちらかに当てはまる場合のみチェックしてください。白い苔が薄くついているのは正常です）</label></div>
												<div class="fd">

													<div class="fbox">
														<div class="bero22-2">
															<div class="beropict">
																<a href="/07/img/bero/zoom/5.jpg" rel="lightbox" title="白苔"><img src="/07/img/bero/5.jpg" alt="" border="0" /></a>
															</div>
															<div class="berocheck">
																<label><input name="bero2[]" type="checkbox" value="白苔" />白苔</label>
															</div>
														</div>

														<div class="bero22-2">
															<div class="beropict">
																<a href="/07/img/bero/zoom/6.jpg" rel="lightbox" title="黄苔"><img src="/07/img/bero/6.jpg" alt="" border="0" /></a>
															</div>
															<div class="berocheck">
																<label><input name="bero2[]" type="checkbox" value="黄苔" />黄苔</label>
															</div>
														</div>
													</div>
												</div>

												<div class="ft"><label for="">22-3：下記の舌に当てはまる場合はチェックしてください。</label></div>
												<div class="fd">

													<div class="fbox">
														<div class="bero22-3">
															<div class="berodesc">舌自体が赤く、ひび割れている</div>
															<div class="beropict">
																<a href="/07/img/bero/zoom/7.jpg" rel="lightbox" title="舌自体が赤く、ひび割れている"><img src="/07/img/bero/7.jpg" alt="" border="0" /></a>
															</div>
															<div class="berocheck">
																<label><input name="bero3[]" type="checkbox" value="裂紋舌" />裂紋舌</label>
															</div>
														</div>

														<div class="bero22-3">
															<div class="berodesc">舌の裏側の静脈がうっ血していて、太い</div>
															<div class="beropict">
																<a href="/07/img/bero/zoom/8.jpg" rel="lightbox" title="舌の裏側の静脈がうっ血していて、太い"><img src="/07/img/bero/8.jpg" alt="" border="0" /></a>
															</div>
															<div class="berocheck">
																<label><input name="bero3[]" type="checkbox" value="舌下怒張" />舌下怒張</label>
															</div>
														</div>
													</div>

												</div>


											</div>

										</div>
										<!-- ↑  -->

										<!-- ↓  -->
										<div class="soudanformlist">

											<div id="soudanform-women">

												<h3>女性の方は答えて下さい。</h3>

												<div class="fbox">
													<div class="ft"><label for="">23. 出産経験はありますか？</label></div>
													<div class="fd">
														<input name="women1[]" type="text" size="5" maxlength="3" />年前　
														<input name="women1[]" type="checkbox" value="ない" />ない
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">24. 生理の周期は？</label></div>
													<div class="fd">
														<label>生理の周期は（<input name="women2[]"type="textbox" />日）</label>
														<label><input name="women2[]" type="checkbox" value="生理が一時的に止まっている" />生理が一時的に止まっている</label>
														<label><input name="women2[]" type="checkbox" value="閉経したor閉経しそうだ" />閉経したor閉経しそうだ</label>
														<label><input name="women2[]" type="checkbox" value="周期が安定していなくバラバラ" />周期が安定していなくバラバラ</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">25. 生理の出血は？</label></div>
													<div class="fd">
														<label class="f70px"><input name="women3[]" type="checkbox" value="多い" />多い</label>
														<label class="f70px"><input name="women3[]" type="checkbox" value="少ない" />少ない</label>
														<label class="f70px"><input name="women3[]" type="checkbox" value="普通" />普通 </label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">26. 生理の期間は？</label></div>
													<div class="fd">
														<label>生理の期間は（<input name="women4" name="" type="textbox" id="" value="" />日）</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">27. 生理痛は？</label></div>
													<div class="fd">
														<label><input name="women5[]" type="checkbox" value="痛みがひどいor鎮痛剤を服用している" />痛みがひどいor鎮痛剤を服用している</label>
														<label><input name="women5[]" type="checkbox" value="ある（薬は必要ない）" />ある（薬は必要ない）</label>
														<label><input name="women5[]" type="checkbox" value="ない" />ない</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">28. 経血の状態は？</label></div>
													<div class="fd">
														<label><input name="women6[]" type="checkbox" value="どろっとした塊りがある" />どろっとした塊りがある</label>
														<label><input name="women6[]" type="checkbox" value="うすくさらさらしている" />うすくさらさらしている</label>
														<label><input name="women6[]" type="checkbox" value="色が茶または黒っぽい" />色が茶または黒っぽい</label>
														<label><input name="women6[]" type="checkbox" value="色が赤く、塊はない" />色が赤く、塊はない</label>
													</div>
												</div>

												<div class="fbox">
													<div class="ft"><label for="">29. 生理前後の状態は？</label></div>
													<div class="fd">
														<label><input name="women7[]" type="checkbox" value="生理前に胸が張ったり、イライラする" />生理前に胸が張ったり、イライラする</label>
														<label><input name="women7[]" type="checkbox" value="普段より眠い、疲れやすい" />普段より眠い、疲れやすい</label>
														<label class="f140px"><input name="women7[]" type="checkbox" value="ニキビが出る" />ニキビが出る</label>
														<label class="f140px"><input name="women7[]" type="checkbox" value="むくむ" />むくむ</label>
														<label class="f140px"><input name="women7[]" type="checkbox" value="下痢する" />下痢する</label>
														<label class="f140px"><input name="women7[]" type="checkbox" value="便秘する" />便秘する</label>
														<label class="f140px"><input name="women7[]" type="checkbox" value="特に症状はない" />特に症状はない</label>
													</div>
												</div>
			
											</div>

										</div>
										<!-- ↑  -->

										<!-- ↓  -->
										<div class="soudanformlist">

											<div id="submit">
												<input type="submit" name="submit" value="送信" />
												<p>
													ご相談内容の確認メールをお送りいたします。確認メールがが届かない場合はご相談メールが正しく送信されていない可能性がありますのでもう一度送信して頂くか、お電話でご連絡ください。
												</p>
											</div>

										</div>
										<!-- ↑  -->

									</form>

								</div>
								<!-- ↑ contents-inner -->

								<!-- ↓ ページトップへ -->
<?
include 'topofpage.html';
?>
								<!-- ↑ ページトップへ -->

							</div>
							<!-- ↑ contents -->

						</div>
						<!-- ↑ fbox-->

					</div>
				</div>
			</div>
			<!-- ↑ main box-->


		</div>
		<!-- ↑ main -->


		<!-- ↓ fot -->
<?
include 'fot.html';
?>
		<!-- ↑ fot -->

	</div>

	<script type="text/javascript">
		function formCallback(result, form) {
			window.status = "valiation callback for form '" + form.id + "': result = " + result;
		}
		var valid = new Validation('form-soudanform', {immediate : true, onFormValidate : formCallback, useTitles:true});
	</script>

</body>
</html>


