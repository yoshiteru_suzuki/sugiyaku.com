<?php
require_once("../../common/php/init.php");

	if($_POST['submit'])
	{
		extract($_POST);

		//いたずら防止
		$str = @$name.@$shoujyou1.@$shoujyou2.@$shoujyou3.@$shoujyou4;
		if(!mb_ereg("[あ-ん]{1,}", $str))
		{
			//complete.phpページへ飛びます。
			header("Location: soudan_form_pet.php");
			exit;
		}

		//テキストフィールドを取得
		$txtdata = array(
												'1. お名前'		=> @$name,
												'2. 電話番号'		=> @$tel1,
												'3. E-Mail'		=> @$email,
												'4. お住まい'	=> @$adrken,
												'5. 性別'	=> @$sex,
												'6. お名前'	=> @$petname,
												'7. 種類'	=> @$shurui,
												'8. 種別'	=> @$shubetsu,
												'9. 年齢'	=> @$nenrei."歳",
												'10. 体重'	=> @$taijyuu."kg",
												'11. 今一番気になる症状は？'	=> @$shoujyou1,
												'12. いつ頃からですか？原因は？'	=> @$shoujyou2,
												'13. その症状が酷くなるのは？（例、朝起きた時、疲れたとき、冬など）'	=> @$shoujyou3,
												'14. . 今までに病気をしたことがありますか？いつ頃？また、現在治療中の病気はありますか？'	=> @$shoujyou4,
												'15. 食欲は？'	=> GetCheckData($_POST['taichou1']),
												'16. 飲物は？'	=> GetCheckData($_POST['taichou2']),
												'17. 便通は？'	=> GetCheckData($_POST['taichou3']),
												'18. 小便は？'	=> GetCheckData($_POST['taichou4']),
												'19. 体質は？'	=> GetCheckData($_POST['taichou5']),
												'20. 出産歴は？'	=> GetCheckData($_POST['mesu1']),
												'21. 生理出血について'	=> GetCheckData($_POST['mesu2']),
												'22. 経血の状態は？'	=> GetCheckData($_POST['mesu3']),
												'23. 現在服用中の薬、サプリメントがあれば教えてください？'	=> @$etc1,
												'24. 処方する際、お望みの剤形があればチェックしてください'	=> GetCheckData($_POST['etc2']),
											);



		// メール送信
		$from_adrress = 'admin@sugiyaku.com';
		$to_adrress_user = @$email;
		$reply = 'info@sugiyaku.com';
		$subject = "【スギヤマ薬局】ご相談受付 タクヤ先生の相談室 「ペット用漢方薬」";

		//日本語をしますよ
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");

		$mail_body = @$name."様\n\n";
		$mail_body .= "ご相談、ありがとうございます。\n";
		$mail_body .= "お送りいただきました相談フォームをもとにお返事をさせていただきます。\n";
		$mail_body .= "こちらからの返信が届くまで今しばらくお待ちくださいませ。\n";
		$mail_body .= "\n";
		$mail_body .= "万一、2日以上ご返信が無い場合はお手数ですがお電話、\nもしくはメール(info@sugiyaku.com)にてお問い合わせください。\nよろしくお願いいたします。\n";
		$mail_body .= "\n";
		$mail_body .= "\n";
		$mail_body .= "============================================================================\n";
		$mail_body .= "漢方のスギヤマ薬局\n";
		$mail_body .= "住所　　：神奈川県座間市相模が丘5-10-37\n";
		$mail_body .= "電話　　：042-746-1951\n";
		$mail_body .= "営業時間：午前9時～午後19時\n";
		$mail_body .= "定休日　：日曜日、祝祭日\n";
		$mail_body .= "E-mail　：info@sugiyaku.com\n";
		$mail_body .= "URL 　　：http://www.sugiyaku.com/\n";
		$mail_body .= "============================================================================\n";


		//相談した人へ
		SendToMail($to_adrress_user, $reply, $subject, $mail_body, $from_adrress, "");


		//杉山薬局へ

		//本文
		$mail_body = "";
		while(list ($key, $val) = each($txtdata)) {
			$mail_body .= "-------------\n$key\n$val\n\n";
			//print "$key, $val<br />";
		}
		SendToMail($to_adrress, $reply, $subject, $mail_body, $from_adrress, "");

		//complete.phpページへ飛びます。
		header("Location: soudan_complete.php");
		exit;

	}

	//メール送信
	function SendToMail( $to, $reply, $subject, $body, $from, $opt )
	{
		$head = "From: ".$from."\n";
		$head .= "Reply-To: ".$reply."\n";
		$head .= "Mime-Version: 1.0\n";
		$head .= "Content-Type: text/plain; charset=ISO-2022-JP\n";
		$head .= "Content-Transfer-Encoding: 7bit\n";
		$opt = $opt;
		mail(
		    $to,
		    mb_encode_mimeheader(mb_convert_encoding($subject, "JIS"), "ISO-2022-JP", "B"),
		    mb_convert_encoding($body, "JIS"),
		    $head,
		    $opt
		);
	}

	//チェックボックス選択アイテム取得
	function GetCheckData($itemname)
	{

		$ret = "";

		for($i = 0; $i < count(@$itemname); $i++)
		{
		    if($i != 0){ $ret .= "・"; }
		    $ret .= $itemname[$i];
		}

		return $ret;

	}
$path = $_SERVER["DOCUMENT_ROOT"]."/";
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>相談フォーム | 漢方相談 スギヤマ薬局</title>
<?php readfile("../../head.html")?>
<link rel="stylesheet" href="/common/lib/jquery.validationEngine/validationEngine.jquery.css" type="text/css">
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine-ja.js"></script>
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine.js"></script>
<script>
$(function() {
	$("#form-soudanform").validationEngine();
});
</script>
</head>
<body id="form" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>相談フォーム</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">相談フォーム</h1>
			<p class="mb30"><a href="/php/policy.html">個人情報の取り扱い &raquo;</a></p>
			<p class="mb30">
				<span class="fb">携帯、スマートフォンをご利用の方へ</span><br>迷惑メール防止のためメールの受信設定をしている場合は､「@sugiyaku.com」を登録、またはドメイン指定解除を行ってください｡<br>
			</p>
			<form name="form-soudanform" id="form-soudanform" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

				<div class="soudanformlist">

					<div id="soudanform-kihon">

						<div class="fbox clearfix">
							<div class="ft"><label>1. お名前</label></div>
							<div class="fd"><input type="text"  name="name" id="name" class="validate[required]" /></div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>2. 電話番号</label></div>
							<div class="fd"><input type="text"  name="tel1" id="tel1" class="validate[required]" /></div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>3. E-Mail</label></div>
							<div class="fd">
								<input type="text" name="email" id="email" class="validate[required, custom[email]]" />
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>4. お住まい</label></div>
							<div class="fd">
								<select name="adrken" id="adrken">
								<option value="北海道">北海道 </option>
								<option value="青森">青森 </option>
								<option value="岩手">岩手 </option>
								<option value="宮城">宮城 </option>
								<option value="秋田">秋田 </option>
								<option value="山形">山形 </option>
								<option value="福島">福島 </option>
								<option value="茨城">茨城 </option>
								<option value="栃木">栃木 </option>
								<option value="群馬">群馬 </option>
								<option value="埼玉">埼玉 </option>
								<option value="千葉">千葉 </option>
								<option value="東京">東京 </option>
								<option value="神奈川">神奈川 </option>
								<option value="新潟">新潟 </option>
								<option value="富山">富山 </option>
								<option value="石川">石川 </option>
								<option value="福井">福井 </option>
								<option value="山梨">山梨 </option>
								<option value="長野">長野 </option>
								<option value="岐阜">岐阜 </option>
								<option value="静岡">静岡 </option>
								<option value="愛知">愛知 </option>
								<option value="三重">三重 </option>
								<option value="滋賀">滋賀 </option>
								<option value="京都">京都 </option>
								<option value="大阪">大阪 </option>
								<option value="兵庫">兵庫 </option>
								<option value="奈良">奈良 </option>
								<option value="和歌山">和歌山 </option>
								<option value="鳥取">鳥取 </option>
								<option value="島根">島根 </option>
								<option value="岡山">岡山 </option>
								<option value="広島">広島 </option>
								<option value="山口">山口 </option>
								<option value="徳島">徳島 </option>
								<option value="香川">香川 </option>
								<option value="愛媛">愛媛 </option>
								<option value="高知">高知 </option>
								<option value="福岡">福岡 </option>
								<option value="佐賀">佐賀 </option>
								<option value="長崎">長崎 </option>
								<option value="熊本">熊本 </option>
								<option value="大分">大分 </option>
								<option value="宮崎">宮崎 </option>
								<option value="鹿児島">鹿児島 </option>
								<option value="沖縄">沖縄 </option>
								<option value="海外" selected="selected">海外</option>
								</select>
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>5. 性別</label></div>
							<div class="fd">
								<select name="sex" id="sex">
								<option value="男性">男性</option>
								<option value="女性" selected="selected">女性</option>
								</select></td>
							</div>
						</div>

						<h2 class="mt20">ペットの情報を入力してください。</h2>

						<div class="fbox clearfix">
							<div class="ft"><label>6. お名前</label></div>
							<div class="fd"><input name="petname" id="petname" type="text" value="" /></div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>7. 種類</label></div>
							<div class="fd"><input name="shurui" id="shurui" type="text" value="" />（犬、猫など）</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>8. 種別</label></div>
							<div class="fd"><input name="shubetsu" id="shubetsu" type="text" value="" /></div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>9. 年齢</label></div>
							<div class="fd"><input name="nenrei" id="nenrei" type="text" value="" /></div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>10. 体重</label></div>
							<div class="fd"><input name="taijyuu" id="taijyuu" type="text" value="" /> kg</div>
						</div>

					</div>

				</div>


				<div class="soudanformlist">

					<div id="soudanform-shoujyou">

						<h2>ご相談内容を入力してください。</h2>

						<div class="ft"><label>11. 今一番気になる症状は？</label></div>
						<div class="fd"><textarea name="shoujyou1" id="shoujyou1" class=""></textarea></div>

						<div class="ft"><label>12. いつ頃からですか？原因は？</label></div>
						<div class="fd"><textarea  name="shoujyou2" id="shoujyou2" class=""></textarea></div>

						<div class="ft"><label>13. その症状が酷くなるのは？（例、朝起きた時、疲れたとき、冬など）</label></div>
						<div class="fd"><textarea name="shoujyou3" id="shoujyou3" class=""></textarea></div>

						<div class="ft"><label>14. 今までに病気をしたことがありますか？いつ頃？また、現在治療中の病気はありますか？</label></div>
						<div class="fd"><textarea name="shoujyou4" id="shoujyou4" class=""></textarea></div>

					</div>

				</div>

				<div class="soudanformlist">

					<div id="soudanform-taichou">

						<h2>現在の体調にあてはまるものをチェックしてください。</h2>

						<div class="fbox clearfix">
							<div class="ft"><label>15. 食欲は？</label></div>
							<div class="fd">
								<div class="fbox">
									<label class="f150px"><input name="taichou1[]" type="checkbox" value="あまり食欲がない" />あまり食欲がない</label>
									<label><input name="taichou1[]" type="checkbox" value="過剰に食べる" />過剰に食べる</label>
								</div>
								<label><input name="taichou1[]" type="checkbox" value="食べても吐いてしまう" />食べても吐いてしまう</label>
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>16. 飲物は？</label></div>
							<div class="fd">
								<div class="fbox">
									<label class="f230px"><input name="taichou2[]" type="checkbox" value="よく水分を取る" />よく水分を取る</label>
									<label ><input name="taichou2[]" type="checkbox" value="あまり水分を摂らない" />あまり水分を摂らない</label>
								</div>
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>17. 便通は？</label></div>
							<div class="fd">
								<div class="fbox">
									<label class="f180px"><input name="taichou3[]" type="checkbox" value="軟便気味だ" />軟便気味だ</label>
									<label class="f180px"><input name="taichou3[]" type="checkbox" value="便秘気味だ" />便秘気味だ</label>
								</div>
								<label class="f180px"><input name="taichou3[]" type="checkbox" value="よく下痢をする" />よく下痢をする</label>
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>18. 小便は？</label></div>
							<div class="fd">
								<div class="fbox">
									<label class="f180px"><input name="taichou4[]" type="checkbox" value="トイレが近い（日中）" />トイレが近い（日中）</label>
									<label class="f180px"><input name="taichou4[]" type="checkbox" value="トイレが近い（夜間）" />トイレが近い（夜間）</label>
								</div>
								<div class="fbox">
									<label class="f180px"><input name="taichou4[]" type="checkbox" value="色が透明だ" />色が透明だ</label>
									<label class="f180px"><input name="taichou4[]" type="checkbox" value="色が濃い" />色が濃い</label>
								</div>
								<label class="f180px"><input name="taichou4[]" type="checkbox" value="あまり小便をしない" />あまり小便をしない</label>
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>19. 体質は？</label></div>
							<div class="fd">
								<div class="fbox">
									<label class="f180px"><input name="taichou5[]" type="checkbox" value="疲れやすい" />疲れやすい</label>
									<label class="f180px"><input name="taichou5[]" type="checkbox" value="むくみやすい" />むくみやすい</label>
								</div>
								<div class="fbox">
									<label class="f180px"><input name="taichou5[]" type="checkbox" value="よく吠える(夜)" />よく吠える(夜)</label>
									<label class="f180px"><input name="taichou5[]" type="checkbox" value="よく吠える(日中)" />よく吠える(日中)</label>
								</div>
								<div class="fbox">
									<label class="f180px"><input name="taichou5[]" type="checkbox" value="円形脱毛がある" />円形脱毛がある</label>
									<label class="f180px"><input name="taichou5[]" type="checkbox" value="鼻が乾いている" />鼻が乾いている</label>
								</div>
								その他<br /><textarea name="taichou5[]"></textarea>
							</div>
						</div>

					</div>

				</div>

				<div class="soudanformlist">

					<div id="soudanform-pet-mesu">

						<h2>雌の場合は答えてください。</h2>

						<div class="fbox clearfix">
							<div class="ft"><label>20. 出産歴は？</label></div>
							<div class="fd">
								<input name="mesu1[]" type="text" size="5" maxlength="3" /> 年前　
								<input name="mesu1[]" type="checkbox" value="ない" />ない
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>21. 生理出血について</label></div>
							<div class="fd">
								<input name="mesu2[]" type="checkbox" value="多い" />多い
								<input name="mesu2[]" type="checkbox" value="少ない" />少ない
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label>22. 経血の状態は？</label></div>
							<div class="fd">
								<div class="fbox">
									<label class="f180px"><input name="mesu3[]" type="checkbox" value="どろっとした塊がある" />どろっとした塊がある</label>
									<label class="f180px"><input name="mesu3[]" type="checkbox" value="うすくさらさらしている" />うすくさらさらしている</label>
								</div>
								<div class="fbox">
									<label class="f180px"><input name="mesu3[]" type="checkbox" value="色が茶　または黒っぽい" />色が茶　または黒っぽい</label>
									<label class="f180px"><input name="mesu3[]" type="checkbox" value="色が赤く、塊はない" />色が赤く、塊はない</label>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="soudanformlist">

					<div id="soudanform-pet-etc">

						<h2>その他</h2>

						<div class="ft mb10"><label>23. 現在服用中の薬、サプリメントがあれば教えてください</label></div>
						<div class="fd mb20"><textarea name="etc1" id="etc1"></textarea></div>

						<div class="ft mb10"><label>24. 処方する際、お望みの剤形があればチェックしてください</label></div>
						<div class="fd">
							<label class="mr10"><input name="etc2[]" type="checkbox" value="錠剤" />錠剤</label>
							<label class="mr10"><input name="etc2[]" type="checkbox" value="散剤" />散剤</label>
							<label class="mr10"><input name="etc2[]" type="checkbox" value="液体" />液体</label>
							<label class="mr10"><input name="etc2[]" type="checkbox" value="とくに指定はない" />とくに指定はない</label>
						</div>
					</div>
				</div>

				<div class="soudanformlist">
					<div class="btn_submit">
						<input type="submit" name="submit" value="送信" />
						<p class="mt10">
							ご相談内容の確認メールをお送りいたします。確認メールがが届かない場合はご相談メールが正しく送信されていない可能性がありますのでもう一度送信して頂くか、お電話でご連絡ください。
						</p>
					</div>
				</div>

			</form>

		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
<?php if(DEBUG) echo '<p id="debug">DEBUG：'.DEBUG.'<br />'.$to_adrress.'</p>'; ?>
</body>
</html>
