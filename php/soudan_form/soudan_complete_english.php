<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Contact us: Finish | Sugiyama Pharmacy</title>
<?php readfile("../../head.html")?>
</head>
<body id="form" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>Contact us</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">Contact us</h1>
			<p class="mb10">Thank you for consultation.<br />We will get back to consultation submitted on the form sent.<br />Please wait until you receive a reply from us.</p>
			<p>If you will not receive e-mail from us more than two days.<br />Please call us or send email to (<a href="mailto:info@sugiyaku.com">info@sugiyaku.com</a>).<br />Thank yo
		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
</body>
</html>
