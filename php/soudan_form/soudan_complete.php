<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>相談フォーム：送信完了 | 漢方相談 スギヤマ薬局</title>
<?php readfile("../../head.html")?>
</head>
<body id="form" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>相談フォーム</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">相談フォーム</h1>
			<p class="mb10">送信いたしました。</p>
			<p class="mb05">ご相談内容の確認メールをお送りいたします。</p>
			<p class="mb05">24時間以上過ぎても確認メールが届かない場合は、ご相談メールが正しく送信されていない可能性がありますので、お手数ですがもう一度送信して頂くか、お電話にてご連絡ください。</p>
		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
</body>
</html>
