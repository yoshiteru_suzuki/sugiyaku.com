<?php
require_once("../../common/php/init.php");

	if($_POST['submit'])
	{
		extract($_POST);

		//テキストフィールドを取得
		$txtdata = array(
												'1. Name'		=> @$name,
												'2. Phone number'		=> @$tel1,
												'3. E-Mail'		=> @$email,
												'4. Address'	=> @$adrken,
												'5. Gender'	=> @$sex,
												'6. Date of birth'	=> @$birth_m." ".@$birth_d.", ".@$birth_y." age:".@$age,
												'7. What is the most symptom that you worry now?'	=> @$shoujyou1
											);

		// メール送信
		$from_adrress = 'admin@sugiyaku.com';
		$to_adrress_user = @$email;
		$reply = 'info@sugiyaku.com';
		$subject = "[ Sugiyama Pharmacy ]";

		//日本語をしますよ
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");

		$mail_body = "Dear Mr./Ms. ".@$name."\n\n";
		$mail_body .= "Thank you for consultation.\n";
		$mail_body .= "We will get back to consultation submitted on the form sent.\n";
		$mail_body .= "Please wait until you receive a reply from us.\n\n";
		$mail_body .= "Thank you.\n";
		$mail_body .= "\n";
		$mail_body .= "\n";
		$mail_body .= "============================================================================\n";
		$mail_body .= "Sugiyama Pharmacy\n";
		$mail_body .= "ADDRESS: Sagamigaoka 5-10-37 Zama-shi Kanagawa 252-0001\n";
		$mail_body .= "Phone: 042-746-1951\n";
		$mail_body .= "Business hours: 9:00a.m. - 7:00p.m.\n";
		$mail_body .= "Regular holiday: Sunday and Holiday\n";
		$mail_body .= "E-mail: info@sugiyaku.com\n";
		$mail_body .= "URL: http://www.sugiyaku.com/\n";
		$mail_body .= "============================================================================\n";


		//相談した人へ
		SendToMail($to_adrress_user, $reply, $subject, $mail_body, $from_adrress, "");


		//杉山薬局へ

		//本文
		$mail_body = "";
		while(list ($key, $val) = each($txtdata)) {
			$mail_body .= "-------------\n$key\n$val\n\n";
		}
		SendToMail($to_adrress, $reply, $subject, $mail_body, $from_adrress, "");

		//complete.phpページへ飛びます。
		header("Location: soudan_complete_english.php");
		exit;

	}

	//メール送信
	function SendToMail( $to, $reply, $subject, $body, $from, $opt )
	{
		$head = "From: ".$from."\n";
		$head .= "Reply-To: ".$reply."\n";
		$head .= "Mime-Version: 1.0\n";
		$head .= "Content-Type: text/plain; charset=ISO-2022-JP\n";
		$head .= "Content-Transfer-Encoding: 7bit\n";
		$opt = $opt;
		mail(
		    $to,
		    mb_encode_mimeheader(mb_convert_encoding($subject, "JIS"), "ISO-2022-JP", "B"),
		    mb_convert_encoding($body, "JIS"),
		    $head,
		    $opt
		);
	}

	//チェックボックス選択アイテム取得
	function GetCheckData($itemname)
	{

		$ret = "";

		for($i = 0; $i < count(@$itemname); $i++)
		{
		    if($i != 0){ $ret .= "・"; }
		    $ret .= $itemname[$i];
		}

		return $ret;

	}


$path = $_SERVER["DOCUMENT_ROOT"]."/";
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Contact us | Sugiyama Pharmacy</title>
<?php readfile("../../head.html")?>
<link rel="stylesheet" href="/common/lib/jquery.validationEngine/validationEngine.jquery.css" type="text/css">
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine-en.js"></script>
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine.js"></script>
<script>
$(function() {
	$("#form-soudanform").validationEngine();
});
</script>
</head>
<body id="form_english" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>Contact us</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">Contact us</h1>

			<form name="form-soudanform" id="form-soudanform" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

				<div class="soudanformlist">

					<div id="soudanform-kihon">

						<div class="fbox clearfix">
							<div class="ft"><label>1. Name</label></div>
							<div class="fd"><input type="text"  name="name" id="name" class="validate[required]" /></div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label for="tel1">2. Phone number</label></div>
							<div class="fd"><input type="text"  name="tel1" id="tel1" class="validate[required]" /></div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label for="email">3. E-Mail</label></div>
							<div class="fd">
								<input type="text" name="email" id="email" class="validate[required,custom[email]]" />
								Please except mobile phone's e-mail.
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label for="adrken">4. Address</label></div>
							<div class="fd">
								<select name="adrken" id="adrken">
								<option value="hokkaido">hokkaido</option>
								<option value="aomori">aomori</option>
								<option value="iwate">iwate</option>
								<option value="miyagi">miyagi</option>
								<option value="akita">akita</option>
								<option value="yamagata">yamagata</option>
								<option value="fukushima">fukushima</option>
								<option value="ibaraki">ibaraki</option>
								<option value="tochigi">tochigi</option>
								<option value="gunma">gunma</option>
								<option value="saitama">saitama</option>
								<option value="chiba">chiba</option>
								<option value="tokyo">tokyo</option>
								<option value="kanagawa">kanagawa</option>
								<option value="niigata">niigata</option>
								<option value="toyama">toyama</option>
								<option value="ishikawa">ishikawa</option>
								<option value="fukui">fukui</option>
								<option value="yamanashi">yamanashi</option>
								<option value="nagano">nagano</option>
								<option value="gifu">gifu</option>
								<option value="shizuoka">shizuoka</option>
								<option value="aichi">aichi</option>
								<option value="mie">mie</option>
								<option value="shiga">shiga</option>
								<option value="kyoto">kyoto</option>
								<option value="osaka">osaka</option>
								<option value="hyogo">hyogo</option>
								<option value="nara">nara</option>
								<option value="wakayama">wakayama</option>
								<option value="tottori">tottori</option>
								<option value="shimane">shimane</option>
								<option value="okayama">okayama</option>
								<option value="hiroshima">hiroshima</option>
								<option value="yamaguchi">yamaguchi</option>
								<option value="tokushima">tokushima</option>
								<option value="kagawa">kagawa</option>
								<option value="ehime">ehime</option>
								<option value="kochi">kochi</option>
								<option value="fukuoka">fukuoka</option>
								<option value="saga">saga</option>
								<option value="nagasaki">nagasaki</option>
								<option value="kumamoto">kumamoto</option>
								<option value="oita">oita</option>
								<option value="miyazaki">miyazaki</option>
								<option value="kagoshima">kagoshima</option>
								<option value="okinawa">okinawa</option>
								<option value="overseas" selected="selected">overseas</option>
								</select>
							</div>
						</div>

						<div class="fbox clearfix">
							<div class="ft"><label for="sex">5. Gender</label></div>
							<div class="fd">
								<select name="sex" id="sex">
								<option value="male">male</option>
								<option value="female" selected="selected">female</option>
								</select>
							</div>
						</div>
						<div class="fbox clearfix">
							<div class="ft"><label>6. Date of birth</label></div>
							<div class="fd">
								<select name="birth_m" id="birth_m">
								<option value="January">January</option>
								<option value="February">February</option>
								<option value="March">March</option>
								<option value="April">April</option>
								<option value="May">May</option>
								<option value="June">June</option>
								<option value="July">July</option>
								<option value="August">August</option>
								<option value="September">September</option>
								<option value="October">October</option>
								<option value="November">November</option>
								<option value="December">December</option>
								</select>
								<select name="birth_d" id="birth_d">
								<option selected="selected">1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
								<option>6</option>
								<option>7</option>
								<option>8</option>
								<option>9</option>
								<option>10</option>
								<option>11</option>
								<option>12</option>
								<option>13</option>
								<option>14</option>
								<option>15</option>
								<option>16</option>
								<option>17</option>
								<option>18</option>
								<option>19</option>
								<option>20</option>
								<option>21</option>
								<option>22</option>
								<option>23</option>
								<option>24</option>
								<option>25</option>
								<option>26</option>
								<option>28</option>
								<option>29</option>
								<option>29</option>
								<option>30</option>
								<option>31</option>
								</select>
								<input type="text" name="birth_y" id="birth_y" value="19" maxlength="4" class="txtbox03"  />
								age: <input type="text" name="age" id="age" size="5" maxlength="3" />
							</div>
						</div>
					</div>

				</div>

				<div class="soudanformlist">
					<div id="soudanform-shoujyou">
						<div class="ft"><label>7. What is the most symptom that you worry now?</label></div>
						<div class="fd"><textarea name="shoujyou1" id="shoujyou1"></textarea></div>
					</div>
				</div>

				<!-- ↓  -->
				<div class="soudanformlist">

					<div class="btn_submit">
						<input type="submit" name="submit" value="submit" />
						<p class="mt05">
							If you will not receive e-mail from us more than two days.<br />Please call us or sent email to (<a href="mailto:info@sugiyaku.com">info@sugiyaku.com</a>).<br />Thank you.

						</p>
					</div>

				</div>
				<!-- ↑  -->

			</form>

		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
<?php if(DEBUG) echo '<p id="debug">DEBUG：'.DEBUG.'<br />'.$to_adrress.'</p>'; ?>
</body>
</html>