<?php
mb_internal_encoding("UTF-8");

$filename = "ranking.txt";

if($_REQUEST["view"] == 1)
{
	ViewRanking($filename);
}
else if($_POST['submit'])
{
	MakeFile($filename);
}
else
{
	ViewForm($filename);
}


// フォーム画面
function ViewForm($filename)
{

	$a_data = array();

	$filepointer=fopen($filename, "r");
	if(!$filepointer)
	{
		print("ファイルオープンエラー<br />");

	}
	else
	{
		rewind($filepointer);
		while(!feof($filepointer)){
		    $fileline = fgets($filepointer);
		    array_push($a_data ,$fileline);
		}
		fclose( $filepointer );

		$title1 = mb_convert_encoding($a_data[0], mb_internal_encoding(), 'auto');
		$url1 	= mb_convert_encoding($a_data[1], mb_internal_encoding(), 'auto');
		$title2 = mb_convert_encoding($a_data[2], mb_internal_encoding(), 'auto');
		$url2 	= mb_convert_encoding($a_data[3], mb_internal_encoding(), 'auto');
		$title3 = mb_convert_encoding($a_data[4], mb_internal_encoding(), 'auto');
		$url3 	= mb_convert_encoding($a_data[5], mb_internal_encoding(), 'auto');
		$title4 = mb_convert_encoding($a_data[6], mb_internal_encoding(), 'auto');
		$url4 	= mb_convert_encoding($a_data[7], mb_internal_encoding(), 'auto');
		$title5 = mb_convert_encoding($a_data[8], mb_internal_encoding(), 'auto');
		$url5 	= mb_convert_encoding($a_data[9], mb_internal_encoding(), 'auto');
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ranking.php</title>
	<link rel="stylesheet" href="ranking.css" type="text/css" media="all" />
</head>
<body>

	<form action="ranking.php" method="post">

		<div id="" class="">
			<h2>1位</h2>
			<label> TITLE：
			<input type="text" value="<?php echo $title1?>" name="title1" class="title" />
			</label>
			<label> URL：
			<input type="text" value="<?php echo $url1?>" name="url1" class="url" />
			</label>
		</div>

		<div id="" class="">
			<h2>2位</h2>
			<label class="title"> TITLE：
			<input type="text" value="<?php echo $title2?>" name="title2" class="title" />
			</label>
			<label> URL：
			<input type="text" value="<?php echo $url2?>" name="url2" class="url" />
			</label>
		</div>

		<div id="" class="">
			<h2>3位</h2>
			<label> TITLE：
			<input type="text" value="<?php echo $title3?>" name="title3" class="title" />
			</label>
			<label> URL：
			<input type="text" value="<?php echo $url3?>" name="url3" class="url" />
			</label>
		</div>

		<div id="" class="">
			<h2>4位</h2>
			<label> TITLE：
			<input type="text" value="<?php echo $title4?>" name="title4" class="title" />
			</label>
			<label> URL：
			<input type="text" value="<?php echo $url4?>" name="url4" class="url" />
			</label>
		</div>

		<div id="" class="">
			<h2>5位</h2>
			<label> TITLE：
			<input type="text" value="<?php echo $title5?>" name="title5" class="title" />
			</label>
			<label> URL：
			<input type="text" value="<?php echo $url5?>" name="url5" class="url" />
			</label>
		</div>

		<div id="">
			<input type="submit" name="submit" value="送信" />
		</div>

	</form>

</body>
</html>

<?php
}

// ファイル書き込み
function MakeFile($filename)
{

	extract($_POST);
	$data = @$title1."\n".@$url1."\n".@$title2."\n".@$url2."\n".@$title3."\n".@$url3."\n".@$title4."\n".@$url4."\n".@$title5."\n".@$url5;

	$filepointer = fopen( $filename, 'w');
	flock($filepointer, LOCK_EX);
	fwrite( $filepointer, $data);
	flock($filepointer, LOCK_UN);
	fclose( $filepointer );
/*

	$htmldata['RANKING'] = '<li id="rank1"><a href="'.$url1.'">'.$title1.'</a></li>';
	$htmldata['RANKING'] .= '<li id="rank2"><a href="'.$url2.'">'.$title2.'</a></li>';
	$htmldata['RANKING'] .= '<li id="rank3"><a href="'.$url3.'">'.$title3.'</a></li>';
	$htmldata['RANKING'] .= '<li id="rank4"><a href="'.$url4.'">'.$title4.'</a></li>';
	$htmldata['RANKING'] .= '<li id="rank5"><a href="'.$url5.'">'.$title5.'</a></li>';

	$output = viewset($htmldata, "subContents.tpl");
	$filepointer = fopen("../../subContents.html", 'w');
	flock($filepointer, LOCK_EX);
	fwrite( $filepointer, $output);
	flock($filepointer, LOCK_UN);
	fclose( $filepointer );
*/

	header("Location: ranking.php");

}


// ランキング表示
function ViewRanking($filename)
{

	$filepointer=fopen($filename, "r");
	if(!$filepointer)
	{

		print("ファイルオープンエラー<br />");

	}
	else
	{

		$a_data = array();

		rewind($filepointer);

		while(!feof($filepointer)){

		    $fileline = fgets($filepointer);

		    // 改行除去
		    $fileline = str_replace("\n", "", $fileline);

				// 配列末尾に追加
		    array_push($a_data ,$fileline);
		}

		fclose( $filepointer );

		$title1 = mb_convert_encoding($a_data[0], mb_internal_encoding(), 'auto');
		$url1 	= mb_convert_encoding($a_data[1], mb_internal_encoding(), 'auto');
		$title2 = mb_convert_encoding($a_data[2], mb_internal_encoding(), 'auto');
		$url2 	= mb_convert_encoding($a_data[3], mb_internal_encoding(), 'auto');
		$title3 = mb_convert_encoding($a_data[4], mb_internal_encoding(), 'auto');
		$url3 	= mb_convert_encoding($a_data[5], mb_internal_encoding(), 'auto');
		$title4 = mb_convert_encoding($a_data[6], mb_internal_encoding(), 'auto');
		$url4 	= mb_convert_encoding($a_data[7], mb_internal_encoding(), 'auto');
		$title5 = mb_convert_encoding($a_data[8], mb_internal_encoding(), 'auto');
		$url5 	= mb_convert_encoding($a_data[9], mb_internal_encoding(), 'auto');

		$ret = '<ul>';
		$ret .= '<li id="rank1"><a href="'.$url1.'">'.$title1.'</a></li>';
		$ret .= '<li id="rank2"><a href="'.$url2.'">'.$title2.'</a></li>';
		$ret .= '<li id="rank3"><a href="'.$url3.'">'.$title3.'</a></li>';
		$ret .= '<li id="rank4"><a href="'.$url4.'">'.$title4.'</a></li>';
		$ret .= '<li id="rank5"><a href="'.$url5.'">'.$title5.'</a></li>';
		$ret .= '</ul>';

	}

	print $ret;

}


// 表示
function viewset($data, $file) {

	$str = file_get_contents($file);

	foreach($data as $key => $val)
	{
		$befor = "/<!--".$key."-->/u";
		$after = $val;
		$str = preg_replace($befor, $after, $str);
	}

	return $str;
}


?>
