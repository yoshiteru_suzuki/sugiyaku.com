<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>商品のご注文・お問い合わせ | 漢方相談 スギヤマ薬局</title>
<?php readfile("../../head.html")?>
<link rel="stylesheet" href="/common/lib/jquery.validationEngine/validationEngine.jquery.css" type="text/css">
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine-ja.js" type="text/javascript"></script>
<script src="/common/lib/jquery.validationEngine/jquery.validationEngine.js" type="text/javascript"></script>
<script>
$(function() {
	$("#form-order").validationEngine();
});
</script>
</head>
<body id="form" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>商品のご注文・お問い合わせ</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">商品のご注文・お問い合わせ</h1>
			<p class="mb20"><a href="/php/policy.html">個人情報の取り扱い &raquo;</a>　<a href="/php/tokushou.html">特定商取引法に基づく表記 &raquo;</a></p>

			<form action="index.php" method="post" id="form-order">

				<h2>お問い合わせ種別</h2>
				<div class="mb20">
					<select name="type">
					<option value="商品（第三類医薬品）のご注文" <?php if(strcmp($DISP["type"], '商品（第三類医薬品）のご注文')==0) echo selected; ?>>商品（第三類医薬品）のご注文</option>
					<option value="商品についてお問い合わせ"<?php if(strcmp($DISP["type"], '商品についてお問い合わせ')==0) echo selected; ?>>商品についてお問い合わせ</option>
					</select>
				</div>

				<h2>商品情報</h2>
				<table class="formTbl01 mb05">
				<tr>
				<th>商品名</th>
				<td><input type="text" name="g_name" value="<?php echo $DISP["g_name"] ?>" /></td></tr>
				<tr>
				<th>数量</th>
				<td><input type="text" name="g_num" value="<?php echo $DISP["g_num"] ?>" maxlength="20"  /></td>
				</tr>
				</table>
				<p class="mb20 fsize10">※商品を複数ご注文の場合は「お問い合わせ内容」の欄に商品名と数量をご記入ください。</p>

				<h2>お客様情報</h2>
				<table class="formTbl01">
				<tr>
				<th>お名前<b class="hiss">*</b></th>
				<td><input type="text" name="name" value="<?php echo $DISP["name"] ?>" class="validate[required]"  /></td></tr>
				<tr><th>フリガナ<b class="hiss">*</b></th>
				<td><input type="text" name="name_kana" value="<?php echo $DISP["name_kana"] ?>" class="validate[required]"  /></td>
				</tr>
				<tr>
				<th>郵便番号<b class="hiss">*</b></th>
				<td>〒 <input type="text" name="zip" value="<?php echo $DISP["zip"] ?>" class="validate[required]"  /></td>
				</tr>
				<tr>
				<th>ご住所<b class="hiss">*</b></th>
				<td><input type="text" name="adr" value="<?php echo $DISP["adr"] ?>" class="validate[required]"  /></td>
				</tr>
				<tr>
				<th>電話番号<b class="hiss">*</b></th>
				<td><input type="text" name="tel1" value="<?php echo $DISP["tel1"] ?>" class="validate[required]"  /></td>
				</tr>
				<tr>
				<th>配送方法<b class="hiss">*</b></th>
				<td>
					<label><input type="radio" name="haisou" value="通常配送" <?php if( strcmp($DISP["haisou"], "通常配送") == 0 ||  empty($DISP["haisou"]) ){ echo "checked='checked'"; } ?>  /> 通常配送</label>　
					<label><input type="radio" name="haisou" value="代引き（＋￥315）" <?php if( strcmp($DISP["haisou"], "代引き（＋￥315）") == 0 ){ echo "checked='checked'"; } ?> /> 代引き（＋￥315）</label>
				</td>
				</tr>
				<tr>
				<th>E-Mail<b class="hiss">*</b></th>
				<td><p>ご注文確認メールをお送り致します。</p><input type="text" name="e_mail" value="<?php echo $DISP["e_mail"] ?>" class="validate[required,custom[email]]"  /></td>
				</tr>
				<tr>
				<th>お問い合わせ内容</th>
				<td><textarea name="remarks" rows="10" class="input-l"><?php echo $DISP["remarks"] ?></textarea></td>
				</tr>
				<tr>
				</tr>
				</table>

				<input type="hidden" name="action" value="confirm" />

				<div class="btn_submit"><input type="submit" name="submit" value="確認画面へ進む" class="input_b"   /></div>
			</form>

		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
</body>
</html>
