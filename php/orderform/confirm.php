<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>商品のご注文・お問い合わせ | 漢方相談 スギヤマ薬局</title>
<?php readfile("../../head.html")?>
</head>
<body id="form" class="under">
<div id="container">
<?php readfile("../../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>商品のご注文・お問い合わせ</span></p>
		<div id="mainContents">
			<h1 class="txt_ttl">商品のご注文・お問い合わせ</h1>

			<form action="index.php" method="post" id="form-order">
				<p class="mb20">下記内容をご確認の上送信してください。</p>

				<input type="hidden" name="type" value="<?php echo $DISP["type"]?>" />
				<input type="hidden" name="g_name" value="<?php echo $DISP["g_name"]?>" />
				<input type="hidden" name="g_num" value="<?php echo $DISP["g_num"]?>" />
				<input type="hidden" name="name" value="<?php echo $DISP["name"]?>" />
				<input type="hidden" name="name_kana" value="<?php echo $DISP["name_kana"]?>" />
				<input type="hidden" name="zip" value="<?php echo $DISP["zip"]?>" />
				<input type="hidden" name="adr" value="<?php echo $DISP["adr"]?>" />
				<input type="hidden" name="tel1" value="<?php echo $DISP["tel1"]?>" />
				<input type="hidden" name="e_mail" value="<?php echo $DISP["e_mail"]?>" />
				<input type="hidden" name="remarks" value="<?php echo $DISP["remarks"]?>" />
				<input type="hidden" name="haisou" value="<?php echo $DISP["haisou"]?>" />

				<h2>お問い合わせ種別</h2>
				<div class="mb20">
					<?php echo $DISP["type"]?>
				</div>


				<h2>商品情報</h2>
				<table class="formTbl01 mb20">
				<tr>
				<th>商品名</th>
				<td><?php if( $DISP["g_name"] ){ echo $DISP["g_name"]; }else{ echo "&nbsp"; } ?></td></tr>
				<tr>
				<th>数量</th>
				<td><?php if( $DISP["g_num"] ){ echo $DISP["g_num"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				</table>

				<h2>お客様情報</h2>
				<table class="formTbl01">
				<tr>
				<th>お名前</th>
				<td><?php if( $DISP["name"] ){ echo $DISP["name"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>フリガナ</th>
				<td><?php if( $DISP["name_kana"] ){ echo $DISP["name_kana"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>郵便番号</th>
				<td>〒 <?php if( $DISP["zip"] ){ echo $DISP["zip"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>ご住所</th>
				<td><?php if( $DISP["adr"] ){ echo $DISP["adr"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>電話番号</th>
				<td><?php if( $DISP["tel1"] ){ echo $DISP["tel1"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>E-Mail</th>
				<td><?php if( $DISP["e_mail"] ){ echo $DISP["e_mail"]; }else{ echo "&nbsp"; } ?></td>
				</tr>
				<tr>
				<th>配送方法</th>
				<td><?php echo $DISP["haisou"]; ?></td>
				</tr>
				<tr>
				<th>お問い合わせ内容</th>
				<td><?php if( $DISP["remarks"] ){ echo nl2br($DISP["remarks"]); }else{ echo "&nbsp"; } ?></textarea></td>
				</tr>
				<tr>
				</tr>
				</table>
				<input type="hidden" name="action" value="complete" />

				<div class="btn_submit">
					<input type="submit" name="submit[back]" value="入力内容を修正" class="input_b"   />
					<input type="submit" name="submit[done]" value="上記の内容で送信" class="input_b"   />
				</div>
			</form>

		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../../footer.html")?>
</div>
</body>
</html>
