<?php

// 追加するファイルの種類
$allow_ext = array(
    "html",
    "txt",
    "php"
);

$baseurl = "http://".$_SERVER["HTTP_HOST"].dirname($_SERVER["PHP_SELF"])."/";

header("Content-type:text/xml;charset=utf-8");

echo <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.google.com/schemas/sitemap/0.84"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">
XML;

if ($dir = @opendir("./")) {
    while (($file = readdir($dir)) !== false) {
        list($tmp, $ext) = explode(".", $file);
        if (in_array($ext, $allow_ext)) {
            $url  = $baseurl.$file;
            $date = date("Y-m-d", filemtime($file));
            echo "<url>\n";
            echo "\t<loc>$url</loc>\n";
            echo "\t<lastmod>$date</lastmod>\n";
            echo "</url>\n";
        }
    }
    closedir($dir);
}

?>
</urlset> 
