/*
 * 共通設定
 */
if (!window.console){ window.console = { log:function(msg){},debug:function(msg){}}; } //debug setting

$().ready(function(){

	setPageAnchorAnimetion();

	/*
	 * ページアンカー
	 */
	function setPageAnchorAnimetion(){

		var href;
		var target;
		var position;
		$(window).on('load',function(){
			$('a[href^=#], .pagetop').click(function() {
				href= $(this).attr("href");
				if(!href){
					position = 0;
				}else{
					target = $(href == "#" || href == "" ? 'html' : href);
					position = target.offset().top+2;
				}
				$('html,body').stop().animate({scrollTop: position}, {duration: 600, easing: "easeInOutExpo"});
				return false;
			});
		});
	}

}); // ready




/*
 * Google Analytics
 */

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-2953667-1', 'auto');
ga('send', 'pageview');
