var ua = navigator.userAgent.toLowerCase(),
    deviceData = {
      isIOS     : false,
      isAndroid : false,
      isTablet  : false,
      isIE      : false,
      device    : "",
      verString : "",
      version   : 0
    };

if (/iphone|ipod|ipad/.test(ua)) {
  deviceData.isIOS  = true;
  
  if (/version/.test(ua)) {
    deviceData.verString = /version\/([^\s]+)/.exec(ua)[1];
  }

  if (/iphone/.test(ua)) {
    deviceData.device = "iPhone";
  } else if (/ipad/.test(ua)) {
    deviceData.isTablet = true;
    deviceData.device = "iPad";
  } else if (/ipod/.test(ua)) {
    deviceData.device = "iPod";
  }
} else if (/android/.test(ua)) {
  deviceData.isAndroid = true;
  deviceData.verString  = /android\s([^;]+)/.exec(ua)[1];

  if (/mobile/.test(ua)) {
    deviceData.device = "Android Mobile";
  } else {
    deviceData.isTablet = true;
    ne.UA.device = "Android Tablet";
  }

} else if (ua.indexOf('msie') >= 0 || ua.indexOf('trident') >= 0) {
    deviceData.isIE = true;
}

if(!deviceData.isTablet && (deviceData.isIOS || deviceData.isAndroid)){
	document.write ('<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">');
}
else{
	document.write ('<meta name="viewport" content="width=1124">');

}

var deviceType, deviceTypeWork;
setDeviceType();
$(window).resize(function(){
	deviceTypeWork = deviceType;
	setDeviceType();
	if(deviceTypeWork != deviceType) location.href=location.href;
});

function setDeviceType(){
	if (window.matchMedia("screen and (min-width:768px)").matches) { 
		deviceType = "desktop";
	} else {
		deviceType = "mobile";
	};
}
