[_tb_system_call storage=system/_scene4sindan.ks]

*start4sindan

[cm  ]
[bg  storage="88145.jpg"  time="1000"  ]
[live2d_new  model_id="ren_game23"  breath="true"  lip_time="100"  jname="ren"  ]
[live2d_show  name="ren_game23"  x="0.06"  y="-1.48"  scale="3.6"  ]
[tb_start_text mode=1 ]
#蓮
[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
中医学のクイズで知識を深めていこう！今回は診断学編をやるよ[p]

[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_start_text mode=1 ]
#蓮
問題が出題されるから、選択肢の中から答えを選んでね[p]
全部で10問あるよ。高得点目指して頑張ろう！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
じゃあ早速はじめるよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
第１問！[p]
舌診で、舌の中央部を観察するのはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="肝"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*肝"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="脾"  x="391"  width="100"  height="30"  y="464"  _clickable_img=""  target="*〇脾"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="腎"  target="*腎"  x="612"  y="464"  width="120"  height="30"  _clickable_img=""  ]
[s  ]
*肝

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm"  ]
*腎

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念！[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm"  ]
*〇脾

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm

[tb_start_text mode=1 ]
第２問！[p]
四肢が冷え、胸腹部に大いに熱がある場合、病証はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="裏熱"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*〇裏熱"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="裏寒"  x="419"  width="100"  height="30"  y="464"  _clickable_img=""  target="*裏寒"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="表裏倶熱"  x="660"  width="100"  height="30"  y="464"  _clickable_img=""  target="*表裏倶熱"  ]
[s  ]
*裏寒

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm2"  ]
*〇裏熱

[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[jump  storage="scene4sindan.ks"  target="*comm2"  ]
*表裏倶熱

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm2

[tb_start_text mode=1 ]
第３問！[p]
次のうち表証と裏証の弁別に重要視されるものはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="悪寒"  target="*〇悪寒"  x="87"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="頭痛"  target="*頭痛"  x="385"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="腹痛"  target="*腹痛"  x="678"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*頭痛

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm3"  ]
*〇悪寒

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm3"  ]
*腹痛

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm3

[tb_start_text mode=1 ]
第４問！[p]
気逆証の病証に属さないものは？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="咳嗽"  target="*咳嗽"  x="101"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="嘔吐"  target="*嘔吐"  x="370"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="尿血"  target="*尿血"  x="634"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*咳嗽

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm4"  ]
*嘔吐

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm4"  ]
*尿血

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm4

[tb_start_text mode=1 ]
第５問！[p]
寒証と熱証を弁別する要点に属さないものは？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="二便の状況"  x="96"  y="464"  width=""  height=""  _clickable_img=""  target="*二便の状況"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="顔の色つや"  x="363"  y="464"  width=""  height=""  _clickable_img=""  target="*顔の色つや"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="話声の高低"  x="623"  y="464"  width=""  height=""  _clickable_img=""  target="*〇話声の高低"  ]
[s  ]
*〇話声の高低

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm5"  ]
*二便の状況

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm5"  ]
*顔の色つや

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm5

[tb_start_text mode=1 ]
第６問！[p]
瞳孔散大が反映される主な病変はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="気血不足"  x="103"  y="464"  width=""  height=""  _clickable_img=""  target="*気血不足"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="腎精耗渇"  x="381"  y="464"  width=""  height=""  _clickable_img=""  target="*〇腎精耗渇"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="肝経風熱"  x="680"  y="464"  width=""  height=""  _clickable_img=""  target="*肝経風熱"  ]
[s  ]
*気血不足

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm6"  ]
*肝経風熱

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm6"  ]
*〇腎精耗渇

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm6

[tb_start_text mode=1 ]
第７問！[p]
気滞が引き起こす血病はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  target="*〇血瘀"  text="血瘀"  x="160"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  target="*出血"  text="出血"  x="408"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  target="*血虚"  text="血虚"  x="632"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*出血

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm7"  ]
*〇血瘀

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm7"  ]
*血虚

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm7

[tb_start_text mode=1 ]
第８問！中気下陥証の症状はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="小便失禁"  x="27"  y="468"  width="214"  height="40"  _clickable_img=""  target="*小便失禁"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="小便が混濁し米の研ぎ汁の様"  x="361"  y="468"  width="188"  height="40"  _clickable_img=""  target="*〇小便が混濁し米の研ぎ汁の様"  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="小便量少、浮腫"  x="652"  y="467"  width="213"  height="40"  _clickable_img=""  target="*小便量少、浮腫"  ]
[s  ]
*小便失禁

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*com8"  ]
*〇小便が混濁し米の研ぎ汁の様

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*com8"  ]
*小便量少、浮腫

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*com8

[tb_start_text mode=1 ]
第９問！[p]
気血不足による疼痛はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="重痛"  target="*重痛"  x="112"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="隠痛"  target="*〇隠痛"  x="394"  y="462"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="絞痛"  x="680"  y="463"  width=""  height=""  _clickable_img=""  target="*絞痛"  ]
[s  ]
*重痛

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm9"  ]
*絞痛

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm9"  ]
*〇隠痛

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm9

[tb_start_text mode=1 ]
最終問題！第１０問　日中に汗が出て、運動すると甚だしくでる汗はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="戦汗"  target="*戦汗"  x="114"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="盗汗"  target="*盗汗"  x="412"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene4sindan.ks"  size="20"  text="自汗"  target="*〇自汗"  x="707"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*盗汗

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm10"  ]
*〇自汗

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene4sindan.ks"  target="*comm10"  ]
*戦汗

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm10

[jump  storage="scene1.ks"  target="*ハッピーエンド"  cond="f.ren_point>60"  ]
[jump  storage="scene1.ks"  target="*ノーマルエンド"  cond="f.ren_point>30"  ]
[jump  storage="scene1.ks"  target="*バッドエンド"  cond="f.ren_point<40"  ]
