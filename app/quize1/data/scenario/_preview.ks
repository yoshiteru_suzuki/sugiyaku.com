[_tb_system_call storage=system/_preview.ks ]

[mask time=10]
[mask_off time=10]
*start

[cm  ]
[bg  storage="88145.jpg"  time="1000"  ]
[live2d_new  model_id="ren_game23"  breath="true"  lip_time="100"  jname="ren"  ]
[live2d_show  name="ren_game23"  x="0.06"  y="-1.48"  scale="3.6"  ]
[tb_start_text mode=1 ]
#蓮
[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#蓮
こんにちは！[p]
中医学のクイズで知識を深めていこう！今回は方剤学をやるよ[p]

[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_start_text mode=1 ]
#蓮
問題が出題されるから、選択肢の中から答えを選んでね[p]
全部で10問あるよ。高得点目指して頑張ろう！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
じゃあ早速はじめるよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
第１問！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
川芎を含まない方剤は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="四物湯"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*しもつ"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="帰脾湯"  x="391"  width="100"  height="30"  y="464"  _clickable_img=""  target="*きひ"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="逍遥散"  x="606"  width="100"  height="30"  y="464"  _clickable_img=""  target="*しょうよう"  ]
[s  ]
*しもつ

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm"  ]
*きひ

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm"  ]
*しょうよう

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念！[p]
[_tb_end_text]

*comm

[tb_start_text mode=1 ]
第２問！[p]
梅核気を治療する代表方剤は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="温胆湯"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*うん"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="半夏厚朴湯"  x="419"  width="100"  height="30"  y="464"  _clickable_img=""  target="*はんげ"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="逍遥散"  x="660"  width="100"  height="30"  y="464"  _clickable_img=""  target="*しょう"  ]
[s  ]
*うん

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm2"  ]
*はんげ

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm2"  ]
*しょう

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm2

[tb_start_text mode=1 ]
第３問！[p]
生姜を含まない方剤はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="小青竜湯"  target="*しょうせい３"  x="87"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="小柴胡湯"  target="*しょうさい３"  x="385"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="桂枝湯"  target="*けいし３"  x="678"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*しょうせい３

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm3"  ]
*しょうさい３

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm3"  ]
*けいし３

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm3

[tb_start_text mode=1 ]
第４問！[p]
風熱感冒の治療でまず選ぶ方剤は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="敗毒散"  target="*はいどく４"  x="101"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="銀翹散"  target="*ぎんぎょう４"  x="370"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="小青竜湯"  target="*しょうせい４"  x="634"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*はいどく４

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm4"  ]
*ぎんぎょう４

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm4"  ]
*しょうせい４

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm4

[tb_start_text mode=1 ]
第５問！気血両虚の眩暈の治療に最も適切な方剤は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="帰脾湯"  x="96"  y="464"  width=""  height=""  _clickable_img=""  target="*きひとう５"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="四物湯"  x="363"  y="464"  width=""  height=""  _clickable_img=""  target="*しもつとう５"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="四君子湯"  x="623"  y="464"  width=""  height=""  _clickable_img=""  target="*しくんしとう５"  ]
[s  ]
*きひとう５

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm5"  ]
*しもつとう５

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm5"  ]
*しくんしとう５

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm5"  ]
*comm5

[tb_start_text mode=1 ]
第６問！次のうち四物湯の構成生薬は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="桂皮"  x="103"  y="464"  width=""  height=""  _clickable_img=""  target="*けいひ６"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="甘草"  x="381"  y="464"  width=""  height=""  _clickable_img=""  target="*かんぞう６"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="地黄"  x="680"  y="464"  width=""  height=""  _clickable_img=""  target="*じおう６"  ]
[s  ]
*けいひ６

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm6"  ]
*かんぞう６

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm6"  ]
*じおう６

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm6

[tb_start_text mode=1 ]
第７問！主な効能を、清養肺胃、降逆下気とする、金匱要略を出典とする方剤は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  target="*百合固金湯"  text="百合固金湯"  x="100"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  target="*瓊玉膏"  text="瓊玉膏"  x="387"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  target="*麦門冬湯"  text="麦門冬湯"  x="632"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*百合固金湯

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm7"  ]
*瓊玉膏

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm7"  ]
*麦門冬湯

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm7

[tb_start_text mode=1 ]
第８問！半夏瀉心湯の中に含まれる生薬はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="人参　乾姜"  x="362"  y="464"  width=""  height=""  _clickable_img=""  target="*人参８"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="厚朴"  x="665"  y="464"  width=""  height=""  _clickable_img=""  target="*厚朴８"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="黄柏"  x="95"  y="464"  width=""  height=""  _clickable_img=""  target="*黄柏"  ]
[s  ]
*人参８

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*com8"  ]
*厚朴８

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*com8"  ]
*黄柏

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*com8

[tb_start_text mode=1 ]
第９問！虚煩心悸、睡眠不安、精神衰疲、夢遺健忘口舌に瘡を生じ、舌紅少苔、の患者に選ぶべき方剤は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="天王補心丹"  target="*てんのうほしんたん９"  x="112"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="酸棗仁湯"  target="*さんそうにんとう９"  x="403"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="知柏地黄丸"  x="696"  y="464"  width=""  height=""  _clickable_img=""  target="*ちばくじおうがん９"  ]
[s  ]
*てんのうほしんたん９

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm9"  ]
*さんそうにんとう９

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm9"  ]
*ちばくじおうがん９

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm9

[tb_start_text mode=1 ]
最終問題！心胸中が大いに寒え痛み、嘔して飲食することあらわず、腹中寒え苔白滑。脈細緊。治療でまず選ぶべきものは？[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="小建中湯"  target="*しょうけんちゅうとう１０"  x="114"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="大建中湯"  target="*だいけんちゅうとう１０"  x="308"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="呉茱萸湯"  target="*呉茱萸湯１０"  x="498"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="理中丸"  target="*理中丸１０"  x="690"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*しょうけんちゅうとう１０

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm10"  ]
*だいけんちゅうとう１０

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm10"  ]
*呉茱萸湯１０

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm10"  ]
*理中丸１０

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm10

[jump  storage="scene1.ks"  target="*ハッピーエンド"  cond="f.ren_point>60"  ]
[jump  storage="scene1.ks"  target="*ノーマルエンド"  cond="f.ren_point>30"  ]
[jump  storage="scene1.ks"  target="*バッドエンド"  cond="f.ren_point<40"  ]
*ハッピーエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="4"  ]
[tb_start_text mode=1 ]
やったね、高得点だ！いつも頑張っている結果が出ているね。お疲れ様[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm11"  ]
*ノーマルエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="2"  ]
[tb_start_text mode=1 ]
まずまずの成績だ。よく頑張っているね。[p]
復習してもう一度チャレンジしてみてね。一緒に頑張ろう![p]

[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm11"  ]
*バッドエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
ランダムで正答してもこの得点は取れないよ。逆に興味がわいてくるな。これを機に一緒に中医学について学んでみない？[p]
[_tb_end_text]

*comm11

[live2d_delete_all  ]
[jump  storage="title_screen.ks"  target=""  cond=""  ]
