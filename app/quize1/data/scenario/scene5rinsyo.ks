[_tb_system_call storage=system/_scene5rinsyo.ks]

*start5rinsyo

[cm  ]
[bg  storage="88145.jpg"  time="1000"  ]
[live2d_new  model_id="ren_game23"  breath="true"  lip_time="100"  jname="ren"  ]
[live2d_show  name="ren_game23"  x="0.06"  y="-1.48"  scale="3.6"  ]
[tb_start_text mode=1 ]
#蓮
[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
中医学のクイズで知識を深めていこう！今回は臨床学（内科）編をやるよ[p]

[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_start_text mode=1 ]
#蓮
問題が出題されるから、選択肢の中から答えを選んでね[p]
全部で10問あるよ。高得点目指して頑張ろう！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
じゃあ早速はじめるよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
第１問！[p]
風寒感冒と風熱感冒の鑑別の根拠とならないものはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="悪寒発熱の軽重"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*悪寒発熱の軽重"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="鼻塞の有無"  x="391"  width="100"  height="30"  y="464"  _clickable_img=""  target="*〇鼻塞の有無"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="咽喉疼痛の有無"  target="*咽喉疼痛の有無"  x="612"  y="464"  width="120"  height="30"  _clickable_img=""  ]
[s  ]
*悪寒発熱の軽重

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm_5"  ]
*咽喉疼痛の有無

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念！[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm_5"  ]
*〇鼻塞の有無

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm_5

[tb_start_text mode=1 ]
第２問！[p]
胃陰不足型の嘔吐の主症でないものはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="腹は減るが食欲なし"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*腹は減るが食欲なし"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="曖気呑酸"  x="419"  width="100"  height="30"  y="464"  _clickable_img=""  target="*〇曖気呑酸"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="繰り返し嘔吐"  x="660"  width="100"  height="30"  y="464"  _clickable_img=""  target="*繰り返し嘔吐"  ]
[s  ]
*腹は減るが食欲なし

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm2_5"  ]
*〇曖気呑酸

[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[jump  storage="scene5rinsyo.ks"  target="*comm2_5"  ]
*繰り返し嘔吐

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm2_5

[tb_start_text mode=1 ]
第３問！[p]
寒邪客胃型の胃痛に対する治法はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="温補脾胃"  target="*温補脾胃"  x="87"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="温中健碑"  target="*温中健碑"  x="385"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="散寒止痛"  target="*〇散寒止痛"  x="678"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*温補脾胃

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm3_5"  ]
*〇散寒止痛

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm3_5"  ]
*温中健碑

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm3_5

[tb_start_text mode=1 ]
第４問！[p]
血虚便秘の主証でないものは？[p]
[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="大便秘結"  target="*大便秘結"  x="101"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="排便時にいきんでも力が入らない"  target="*〇排便時にいきんでも力が入らない"  x="270"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="目眩、心悸"  target="*目眩、心悸"  x="682"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*大便秘結

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm4_5"  ]
*目眩、心悸

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm4_5"  ]
*〇排便時にいきんでも力が入らない

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm4_5

[tb_start_text mode=1 ]
第５問！[p]
背中の痒みが10年続く。掻くと皮屑が落ち、分泌物がなく、皮膚が厚くなる。乾燥している。この弁証はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="風勝"  x="96"  y="464"  width=""  height=""  _clickable_img=""  target="*風勝"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="湿勝"  x="363"  y="464"  width=""  height=""  _clickable_img=""  target="*湿勝"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="血虚"  x="623"  y="464"  width=""  height=""  _clickable_img=""  target="*血虚5"  ]
[s  ]
*血虚5

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm5_5"  ]
*風勝

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm5_5"  ]
*湿勝

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm5_5

[tb_start_text mode=1 ]
第６問！[p]
眩暈、動くと悪化する、顔色蒼白、心悸少寝、話したくない、飲食減少、舌淡、脈細弱。この弁証はどれか？[p]

[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="肝陽上亢"  x="103"  y="464"  width=""  height=""  _clickable_img=""  target="*肝陽上亢6"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="気血両虚"  x="381"  y="464"  width=""  height=""  _clickable_img=""  target="*気血両虚6"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="痰濁中阻"  x="680"  y="464"  width=""  height=""  _clickable_img=""  target="*痰濁中阻6"  ]
[s  ]
*肝陽上亢6

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm6_5"  ]
*痰濁中阻6

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm6_5"  ]
*気血両虚6

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm6_5

[tb_start_text mode=1 ]
第７問！[p]
月経周期が短い、出血量が多く、色は深紅、質粘調、口渇煩熱、この診断はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  target="*〇月経先期の実熱証"  text="月経先期の実熱証"  x="90"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  target="*月経先期の虚熱証"  text="月経先期の虚熱証"  x="360"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  target="*月経先期の鬱熱証"  text="月経先期の鬱熱証"  x="624"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*月経先期の虚熱証

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm7_5"  ]
*〇月経先期の実熱証

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm7_5"  ]
*月経先期の鬱熱証

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm7_5

[tb_start_text mode=1 ]
第８問！気血両虚の眩暈の治療に最も適切な方剤はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="帰脾湯"  x="27"  y="468"  width="214"  height="20"  _clickable_img=""  target="*帰脾湯8"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="補中益気湯"  x="361"  y="468"  width="188"  height="20"  _clickable_img=""  target="*補中益気湯8"  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="当帰補血湯"  x="652"  y="467"  width="180"  height="20"  _clickable_img=""  target="*当帰補血湯"  ]
[s  ]
*補中益気湯8

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*com8_5"  ]
*帰脾湯8

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*com8_5"  ]
*当帰補血湯

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*com8_5

[tb_start_text mode=1 ]
第９問！[p]
ぼうっとするほどの眩暈・頭重で、頭目脹痛、心煩口苦、喉は渇くが飲みたくない、舌苔黄膩、脈弦滑。属する証はどれか？[p]

[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="痰熱中阻"  target="*〇痰熱中阻"  x="112"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="肝陽上亢"  target="*肝陽上亢9"  x="394"  y="462"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="瘀血化熱"  x="680"  y="463"  width=""  height=""  _clickable_img=""  target="*瘀血化熱"  ]
[s  ]
*肝陽上亢9

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm9_5"  ]
*瘀血化熱

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm9_5"  ]
*〇痰熱中阻

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm9_5

[tb_start_text mode=1 ]
最終問題！第１０問　男性、65歳。久病で体が弱っている、平素から風寒に耐えられず、感冒を患いやすい。ここ半月、自汗、悪風あり、動くと悪化する。疲れやすい、面色少華、苔薄白、脈細弱。治療で選ぶべき方剤はどれか？[p]

[_tb_end_text]

[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="桂枝湯"  target="*桂枝湯10"  x="114"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="玉屏風散"  target="*〇玉屏風散"  x="412"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene5rinsyo.ks"  size="20"  text="参附湯"  target="*参附湯"  x="707"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*桂枝湯10

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm10_5"  ]
*〇玉屏風散

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene5rinsyo.ks"  target="*comm10_5"  ]
*参附湯

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm10_5

[jump  storage="scene1.ks"  target="*ハッピーエンド"  cond="f.ren_point>60"  ]
[jump  storage="scene1.ks"  target="*ノーマルエンド"  cond="f.ren_point>30"  ]
[jump  storage="scene1.ks"  target="*バッドエンド"  cond="f.ren_point<40"  ]
