[_tb_system_call storage=system/_scene3syouyaku.ks]

*startsyouyaku

[cm  ]
[bg  storage="88145.jpg"  time="1000"  ]
[live2d_new  model_id="ren_game23"  breath="true"  lip_time="100"  jname="ren"  ]
[live2d_show  name="ren_game23"  x="0.06"  y="-1.48"  scale="3.6"  ]
[tb_start_text mode=1 ]
#蓮
[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
中医学のクイズで知識を深めていこう！今回は生薬学編をやるよ[p]

[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_start_text mode=1 ]
#蓮
問題が出題されるから、選択肢の中から答えを選んでね[p]
全部で10問あるよ。高得点目指して頑張ろう！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
じゃあ早速はじめるよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
第１問！[p]
解表には生で使い、平喘には炙用するのが適する薬は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="甘草"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*甘草"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="荊芥"  x="391"  width="100"  height="30"  y="464"  _clickable_img=""  target="*荊芥１"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="麻黄"  target="*〇麻黄"  x="612"  y="464"  width="120"  height="30"  _clickable_img=""  ]
[s  ]
*甘草

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm"  ]
*荊芥１

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念！[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm"  ]
*〇麻黄

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm

[tb_start_text mode=1 ]
第２問！[p]
生姜の効能でないものはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="発汗解表"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*発汗解表"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="和中利水"  x="419"  width="100"  height="30"  y="464"  _clickable_img=""  target="*〇和中利水"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="温肺止咳"  x="660"  width="100"  height="30"  y="464"  _clickable_img=""  target="*温肺止咳"  ]
[s  ]
*発汗解表

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm2"  ]
[jump  storage="scene3syouyaku.ks"  target="*comm2"  ]
*〇和中利水

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm2"  ]
*温肺止咳

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm2

[tb_start_text mode=1 ]
第３問！[p]
鼻淵鼻塞と頭痛の治療に優れた薬は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="紫蘇"  target="*紫蘇"  x="87"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="細辛"  target="*〇細辛"  x="385"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="桑葉"  target="*桑葉"  x="678"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*紫蘇

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm3"  ]
*〇細辛

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm3"  ]
*桑葉

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm3

[tb_start_text mode=1 ]
第４問！[p]
大黄の主治はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="腸燥便秘"  target="*腸燥便秘"  x="101"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="血虚便秘"  target="*血虚便秘"  x="370"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="熱結便秘"  target="*〇熱結便秘"  x="634"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*腸燥便秘

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm4"  ]
*血虚便秘

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm4"  ]
*〇熱結便秘

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm4

[tb_start_text mode=1 ]
第５問！[p]
厚朴の主治証でないものはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="瘀血内阻"  x="96"  y="464"  width=""  height=""  _clickable_img=""  target="*〇瘀血内阻"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="食積脹満"  x="363"  y="464"  width=""  height=""  _clickable_img=""  target="*食積脹満"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="痰飲喘咳"  x="623"  y="464"  width=""  height=""  _clickable_img=""  target="*痰飲喘咳"  ]
[s  ]
*〇瘀血内阻

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm5"  ]
*食積脹満

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm5"  ]
*痰飲喘咳

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm5

[tb_start_text mode=1 ]
第６問！[p]
水飲心悸の治療に選ぶべき薬は？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="猪苓"  x="103"  y="464"  width=""  height=""  _clickable_img=""  target="*猪苓"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="木通"  x="381"  y="464"  width=""  height=""  _clickable_img=""  target="*木通"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="茯苓"  x="680"  y="464"  width=""  height=""  _clickable_img=""  target="*〇茯苓"  ]
[s  ]
*猪苓

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm6"  ]
*木通

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm6"  ]
*〇茯苓

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm6

[tb_start_text mode=1 ]
第７問！[p]
人参の適応症でないものは？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  target="*陽痿"  text="陽痿"  x="160"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  target="*〇陰虚骨蒸"  text="陰虚骨蒸"  x="367"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  target="*肺虚喘促"  text="肺虚喘促"  x="632"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*陽痿

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm7"  ]
*〇陰虚骨蒸

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm7"  ]
*肺虚喘促

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm7

[tb_start_text mode=1 ]
第８問！葛根の効能はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="発表透疹、清熱解毒、昇陽挙陥"  x="27"  y="468"  width="214"  height="40"  _clickable_img=""  target="*発表透疹、清熱解毒、昇陽挙陥_"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="発表解肌、透疹、生津止渇、昇陽止瀉"  x="361"  y="468"  width="188"  height="40"  _clickable_img=""  target="*〇発表解肌、透疹、生津止渇、昇陽止瀉"  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="疏散風熱、透疹止痒、熄風止痙"  x="652"  y="467"  width="213"  height="40"  _clickable_img=""  target="*疏散風熱、透疹止痒、熄風止痙"  ]
[s  ]
*発表透疹、清熱解毒、昇陽挙陥_

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*com8"  ]
*〇発表解肌、透疹、生津止渇、昇陽止瀉

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*com8"  ]
*疏散風熱、透疹止痒、熄風止痙

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*com8

[tb_start_text mode=1 ]
第９問！[p]
沢瀉の効能はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="清肺火"  target="*清肺火"  x="112"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="清心火"  target="*清心火"  x="394"  y="462"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="清腎火"  x="680"  y="463"  width=""  height=""  _clickable_img=""  target="*〇清腎火"  ]
[s  ]
*清肺火

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm9"  ]
*清心火

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm9"  ]
*〇清腎火

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm9

[tb_start_text mode=1 ]
最終問題！第１０問[p]
桃仁の効能はどれか？[p]

[_tb_end_text]

[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="活血行気"  target="*活血行気"  x="114"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="活血潤腸"  target="*〇活血潤腸"  x="412"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene3syouyaku.ks"  size="20"  text="活血涼血"  target="*活血涼血"  x="707"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*活血行気

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm10"  ]
*〇活血潤腸

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene3syouyaku.ks"  target="*comm10"  ]
*活血涼血

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm10

[jump  storage="scene1.ks"  target="*ハッピーエンド"  cond="f.ren_point>60"  ]
[jump  storage="scene1.ks"  target="*ノーマルエンド"  cond="f.ren_point>30"  ]
[jump  storage="scene1.ks"  target="*バッドエンド"  cond="f.ren_point<40"  ]
*ハッピーエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="4"  ]
[tb_start_text mode=1 ]
やったね、高得点だ！いつも頑張っている結果が出ているね。お疲れ様[p]
[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm11"  ]
*ノーマルエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="2"  ]
[tb_start_text mode=1 ]
まずまずの成績だ。よく頑張っているね。[p]
復習してもう一度チャレンジしてみてね。一緒に頑張ろう![p]

[_tb_end_text]

[jump  storage="scene1.ks"  target="*comm11"  ]
*バッドエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
ランダムで正答してもこの得点は取れないよ。逆に興味がわいてくるな。これを機に一緒に中医学について学んでみない？[p]
[_tb_end_text]

*comm11

[live2d_delete_all  ]
[jump  storage="title_screen.ks"  target=""  cond=""  ]
