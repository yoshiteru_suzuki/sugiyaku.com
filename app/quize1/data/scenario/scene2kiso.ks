[_tb_system_call storage=system/_scene2kiso.ks]

*startkiso

[cm  ]
[bg  storage="88145.jpg"  time="1000"  ]
[live2d_new  model_id="ren_game23"  breath="true"  lip_time="100"  jname="ren"  ]
[live2d_show  name="ren_game23"  x="0.06"  y="-1.48"  scale="3.6"  ]
[tb_start_text mode=1 ]
#蓮
[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#蓮
中医学のクイズで知識を深めていこう！今回は中医学基礎をやるよ！[p]

[_tb_end_text]

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="0"  ]
[tb_start_text mode=1 ]
#蓮
問題が出題されるから、選択肢の中から答えを選んでね[p]
全部で10問あるよ。高得点目指して頑張ろう！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
じゃあ早速はじめるよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
第１問！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
肺の清気を吸い込む機能は主にどれによるものか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="肺は呼吸を主る"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*こきゅう"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="肺は皮毛に精を輸する"  x="391"  width="100"  height="30"  y="464"  _clickable_img=""  target="*ひもう"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="肺は宣発と粛降を主る"  target="*せんぱつ"  x="611"  y="457"  width="120"  height="40"  _clickable_img=""  ]
[s  ]
*こきゅう

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm"  ]
*ひもう

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念！[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm"  ]
*せんぱつ

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm

[tb_start_text mode=1 ]
第２問！[p]
腎気不固の症状に属さないものは？[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="浮腫"  x="161"  width="100"  height="30"  y="464"  _clickable_img=""  target="*〇ふしゅ"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="遺精"  x="419"  width="100"  height="30"  y="464"  _clickable_img=""  target="*遺精"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="小便失禁"  x="660"  width="100"  height="30"  y="464"  _clickable_img=""  target="*小便失禁"  ]
[s  ]
*〇ふしゅ

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm2"  ]
*遺精

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm2"  ]
*小便失禁

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm2

[tb_start_text mode=1 ]
第３問！[p]
女性の月経や男性の射精を調整する臓はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="脾腎"  target="*ひじん"  x="87"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="心腎"  target="*しんじん"  x="385"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="肝腎"  target="*かんじん"  x="678"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*ひじん

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm3"  ]
*しんじん

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm3"  ]
*かんじん

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm3

[tb_start_text mode=1 ]
第４問！[p]
「受盛」の効能を有する臓器はどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="脾"  target="*脾"  x="101"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="胃"  target="*胃"  x="370"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="小腸"  target="*小腸"  x="634"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*脾

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm4"  ]
*胃

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm4"  ]
*小腸

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm4

[tb_start_text mode=1 ]
第５問！[p]
「気虚湿盛」の体質の人が邪を受けた後、変化しやすいのはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="水化"  x="96"  y="464"  width=""  height=""  _clickable_img=""  target="*すいか"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="寒化"  x="363"  y="464"  width=""  height=""  _clickable_img=""  target="*かんか"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="湿化"  x="623"  y="464"  width=""  height=""  _clickable_img=""  target="*しつか"  ]
[s  ]
*すいか

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm5"  ]
*かんか

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm5"  ]
*しつか

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm5"  ]
*comm5

[tb_start_text mode=1 ]
第６問！[p]
「血海」と称される経脈はどれか[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="衝脈"  x="103"  y="464"  width=""  height=""  _clickable_img=""  target="*〇衝脈_"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="任脈"  x="381"  y="464"  width=""  height=""  _clickable_img=""  target="*任脈"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="足陽明経"  x="680"  y="464"  width=""  height=""  _clickable_img=""  target="*足陽明経"  ]
[s  ]
*〇衝脈_

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm6"  ]
*任脈

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm6"  ]
*足陽明経

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm6

[tb_start_text mode=1 ]
第７問！出血に至らないのはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  target="*血瘀"  text="血瘀"  x="100"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  target="*気虚"  text="気虚"  x="387"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  target="*〇血寒"  text="血寒"  x="632"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*血瘀

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm7"  ]
*気虚

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm7"  ]
*〇血寒

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

*comm7

[tb_start_text mode=1 ]
第８問！「寒因寒用」の治方はどの治療に適用するか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="真虚仮実証"  x="362"  y="464"  width=""  height=""  _clickable_img=""  target="*〇真虚仮実証"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="表虚裏実証"  x="665"  y="464"  width=""  height=""  _clickable_img=""  target="*表虚裏実証"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="真実仮虚証"  x="95"  y="464"  width=""  height=""  _clickable_img=""  target="*真実仮実証"  ]
[s  ]
*〇真虚仮実証

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*com8"  ]
*表虚裏実証

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*com8"  ]
*真実仮実証

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*com8

[tb_start_text mode=1 ]
第９問！[p]
五臓の中で、昇を以って健とするのはどれか？[p]
[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="心"  target="*心"  x="112"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="肝"  target="*肝"  x="267"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="脾"  x="413"  y="464"  width=""  height=""  _clickable_img=""  target="*〇脾"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="肺"  x="585"  y="465"  width=""  height=""  _clickable_img=""  target="*肺"  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="腎"  x="739"  y="466"  width=""  height=""  _clickable_img=""  target="*腎"  ]
[s  ]
*心

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm9"  ]
*肝

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm9"  ]
*〇脾

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm9"  ]
*肺

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm9"  ]
*腎

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm9

[tb_start_text mode=1 ]
最終問題！第１０問[p]
のどに異物感があり、飲み込むことも吐き出すこともできない。情志の抑鬱感、ため息、少腹が脹満して移動性の痛みがある。この病機はどれか？[p]

[_tb_end_text]

[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="腎気不固"  target="*腎気不固"  x="114"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="肝気鬱血"  target="*〇肝気鬱血"  x="395"  y="464"  width=""  height=""  _clickable_img=""  ]
[glink  color="black"  storage="scene2kiso.ks"  size="20"  text="胃腸気滞"  target="*胃腸気滞"  x="686"  y="464"  width=""  height=""  _clickable_img=""  ]
[s  ]
*腎気不固

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm10"  ]
*〇肝気鬱血

[tb_eval  exp="f.ren_point+=10"  name="ren_point"  cmd="+="  op="t"  val="10"  val_2="undefined"  ]
[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="3"  ]
[tb_start_text mode=1 ]
正解！[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm10"  ]
*胃腸気滞

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
残念[p]
[_tb_end_text]

*comm10

[jump  storage="scene2kiso.ks"  target="*ハッピーエンド"  cond="f.ren_point>60"  ]
[jump  storage="scene2kiso.ks"  target="*ノーマルエンド"  cond="f.ren_point>30"  ]
[jump  storage="scene2kiso.ks"  target="*バッドエンド"  cond="f.ren_point<40"  ]
*ハッピーエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="4"  ]
[tb_start_text mode=1 ]
やったね、高得点だ！いつも頑張っている結果が出ているね。お疲れ様[p]
[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm11"  ]
*ノーマルエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="2"  ]
[tb_start_text mode=1 ]
まずまずの成績だ。よく頑張っているね。[p]
復習してもう一度チャレンジしてみてね。一緒に頑張ろう![p]

[_tb_end_text]

[jump  storage="scene2kiso.ks"  target="*comm11"  ]
*バッドエンド

[live2d_motion  name="ren_game23"  mtn="Tapbody"  no="1"  ]
[tb_start_text mode=1 ]
ランダムで正答してもこの得点は取れないよ。逆に興味がわいてくるな。これを機に一緒に中医学について学んでみない？[p]
[_tb_end_text]

*comm11

[live2d_delete_all  ]
[jump  storage="title_screen.ks"  target=""  cond=""  ]
