<?php
$atom = simplexml_load_file("https://www.sugiyaku.com/takuya_b/atom.xml");
$cnt=0;
$takuya_b="";
$a_kenkou = array();
foreach($atom->{'entry'} as $item){
	$attr = $item->{'link'}->attributes();
	$link = $attr['href'];
	$title = $item->title;
	$pt = $item ->published;
	$pt = date("Y/m/d", strtotime($pt,+9000));
	$takuya_b .= "<dt>$pt</dt><dd><a href=\"$link\">$title</a></dd>\n";
	$cnt++;
	if($cnt>=3) break;
}

function kenkou($atom)
{
	$cnt=0;
	foreach($atom->{'entry'} as $item){
		$attr = $item->{'link'}->attributes();
		$link = $attr['href'];
		$title = $item->title;
		$pt = $item ->published;
		$pt = date("Y/m/d", strtotime($pt,+9000));
		$ret[$cnt]['date'] = date("Ymd", strtotime($pt,+9000));
		$ret[$cnt]['tag'] = "<dt>$pt</dt><dd><a href=\"$link\">$title</a></dd>\n";
		$cnt++;
		if($cnt>=3) break;
	}
	return $ret;
}

function get_kenkou($a_kenkou)
{
	$kokoro = kenkou(simplexml_load_file("http://www.sugiyaku.com/soudan/kokoro/atom.xml"));
	$health = kenkou(simplexml_load_file("http://www.sugiyaku.com/soudan/health/atom.xml"));
	$beauty = kenkou(simplexml_load_file("http://www.sugiyaku.com/soudan/beauty/atom.xml"));
	$agenesis = kenkou(simplexml_load_file("http://www.sugiyaku.com/soudan/agenesis/atom.xml"));
	$kids = kenkou(simplexml_load_file("http://www.sugiyaku.com/soudan/kids/atom.xml"));
	$cancer = kenkou(simplexml_load_file("http://www.sugiyaku.com/soudan/cancer/atom.xml"));
	$pet = kenkou(simplexml_load_file("http://www.sugiyaku.com/soudan/pet/atom.xml"));

	$a_kenkou = array_merge($health, $beauty);
	$a_kenkou = array_merge($a_kenkou, $agenesis);
	$a_kenkou = array_merge($a_kenkou, $kids);
	$a_kenkou = array_merge($a_kenkou, $cancer);
	$a_kenkou = array_merge($a_kenkou, $pet);
	$a_kenkou = array_merge($a_kenkou, $kokoro);

	foreach($a_kenkou as $key=>$value){
		$id[$key] = $value['date'];
	}

	array_multisort($id ,SORT_DESC, $a_kenkou);

	$cnt=0;
	foreach($a_kenkou as $item){
		echo $item['tag'];
		$cnt++;
		if($cnt>=3) break;
	}
}


?>

				<div id="takuya">
					<div class="mi">タクヤブログ</div>
					<dl>
<?php echo $takuya_b; ?>
					</dl>
				</div>
				<div id="kenkou">
					<div class="mi">健康情報</div>
					<dl>
<?php get_kenkou($a_kenkou) ?>
					</dl>
				</div>
