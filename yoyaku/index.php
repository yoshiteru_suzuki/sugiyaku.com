<?php
if (!empty($_GET['test'])) {
	require_once("test.php");
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>ご相談予約 総合受付 | 漢方相談 スギヤマ薬局</title>
	<?php readfile("../head.html") ?>
	<link rel="stylesheet" type="text/css" href="/assets/css/yoyaku.css" />
	<script src="/assets/js/yoyaku.js?r=1"></script>
	<script>
		$(function() {
			$('#main_pic').crossSlide({
				sleep: 4,
				fade: 1
			}, [{
					src: '../info/images/staff_visual.jpg'
				},
				{
					src: 'images/idx_visual1.jpg'
				}
			]);
		});
	</script>
</head>

<body id="idx" class="under yoyaku">
	<div id="container">
		<?php readfile("../header.html") ?>
		<div id="contents" class="inner clearfix">
			<p id="bread"><a href="/" id="bread_home">HOME</a><span>ご相談予約 総合受付</span></p>
			<div id="mainContents" class="clearfix">
				<div id="main_visual">
					<h1 id="main_ttl"><img src="images/idx_ttl.gif" alt="ご相談予約 総合受付" /></h1>
					<div id="main_pic">
						loading..
					</div>
				</div>
				<div class="read">
					<p>
						スギヤマ薬局では、お一人様にきちんとお時間を取り、ゆっくりとご相談いただけるよう、ご予約をいただいております。お手数ですが、相談日時のご予約にご協力をお願いいいたします。
					</p>
				</div>
				<div class="box1">
					<p class="note1">相談のみの場合は2,000円/30分の相談料を頂戴いたします。</p>
				</div>
				<div class="read2">
					<p>
						下記フォームに必要事項ご入力の上、送信してください。確認でき次第、予約時間についてメールかお電話にてご連絡させていただきます。<br>お急ぎの方は店舗に直接お電話にてご連絡ください。
					</p>
					<p class="required">マークは必須項目です。</p>
					<p>（わかる範囲で構いませんので、必須項目以外もなるべくご入力ください）</p>
				</div>
				<div class="entry">
					<form class="form-yoyaku" id="form-yoyaku" action="send.php" method="post">
						<div class="box-type">
							<dl>
								<dt class="step">
									<span>1</span>
									ご相談方法を選択してください。
								</dt>
								<dd>
									<div>
										<label><input type="radio" name="houhou" value="ご来店">ご来店</label>
										<label><input type="radio" name="houhou" value="スカイプ">スカイプ</label>
										<label><input type="radio" name="houhou" value="お電話">お電話</label>
									</div>
								</dd>
								<dt class="step">
									<span>2</span>
									ご相談される方を選択してください。
								</dt>
								<dd>
									<div>
										<label><input type="radio" name="target" value="赤ちゃん">赤ちゃん</label>
										<label><input type="radio" name="target" value="お子様">お子様</label>
										<label><input type="radio" name="target" value="大人">大人</label>
										<label><input type="radio" name="target" value="ペット">ペット</label>
									</div>
								</dd>
								<dt class="step">
									<span>3</span>
									ご相談は初めてですか？
								</dt>
								<dd>
									<div>
										<label><input type="radio" name="hajimete" value="初めて">初めて</label>
										<label><input type="radio" name="hajimete" value="2回目以降">2回目以降</label>
										<label><input type="radio" name="hajimete" value="お久しぶり">お久しぶり（前回が半年以上前）</label>
									</div>
									<p class="hajimete-note box1 step2">情報にお変わりのない場合、<span>5</span>以降のお身体の状態などは省略していただいても結構です。</p>
								</dd>
							</dl>
						</div>
						<section class="box-base">
							<h2 class="step">
								<span>4</span>
								基本情報を入力してください。
							</h2>
							<dl class="form-list">
								<dt>お名前</dt>
								<dd>
									<div class="required">
										<input type="text" size="100" name="name" required value="<?php echo $name; ?>">
									</div>
								</dd>
								<dt>フリガナ</dt>
								<dd>
									<div class="required">
										<input type="text" size="100" name="name-kana" required value="<?php echo $namekana; ?>">
									</div>
								</dd>
								<dt>性別</dt>
								<dd>
									<div class="sex required">
										<select class="" name="sex" id="sex" required>
											<option value="">選択してください</option>
											<option value="男性">男性</option>
											<option value="女性">女性</option>
										</select>
									</div>
								</dd>
								<dt>年齢</dt>
								<dd>
									<div class="age required">
										<input type="text" size="20" name="age" value="<?php echo $age; ?>" required>
									</div>
								</dd>
								<dt>郵便番号</dt>
								<dd>
									<div class="adr required">
										<input type="text" size="20" name="postal-code" value="<?php echo $postalcode; ?>" required>
									</div>
								</dd>
								<dt>都道府県</dt>
								<dd>
									<div class="adr required">
										<input type="text" size="100" name="address-level1" value="<?php echo $addresslevel1; ?>" required>
									</div>
								</dd>
								<dt>市区町村</dt>
								<dd>
									<div class="adr required">
										<input type="text" size="100" name="address-level2" value="<?php echo $addresslevel2; ?>" required>
									</div>
								</dd>
								<dt>番地</dt>
								<dd>
									<div class="adr required">
										<input type="text" size="100" name="address-line1" value="<?php echo $addressline1; ?>" required>
									</div>
								</dd>
								<dt>マンション名</dt>
								<dd><input type="text" size="100" name="address-line2" value="<?php echo $addressline2; ?>"></dd>
								<dt>電話</dt>
								<dd>
									<div class="required">
										<input type="text" size="100" name="tel" required value="<?php echo $tel; ?>">
									</div>
								</dd>
								<dt>メールアドレス</dt>
								<dd>
									<div class="required">
										<input type="email" size="100" name="email" required value="<?php echo $email; ?>">
									</div>
								</dd>
								<dt>スカイプID</dt>
								<dd>
									<div>
										<input type="text" size="100" name="skypeid" value="<?php echo $skypeid; ?>">
									</div>
								</dd>
								<dt>予約日時（第1希望）</dt>
								<dd>
									<div class="required">
										<input type="date" size="50" name="yoyakubi1_date" value="<?php echo $yoyakubi1_date; ?>" required>
										<select name="yoyakubi1_time" required>
											<option value="">時間</option>
											<option value="10:00">10:00</option>
											<option value="11:00">11:00</option>
											<option value="12:00">12:00</option>
											<option value="13:00">13:00</option>
											<option value="14:00">14:00</option>
											<option value="15:00">15:00</option>
											<option value="16:00">16:00</option>
											<option value="17:00">17:00</option>
											<option value="18:00">18:00</option>
										</select>
										<p class="fsize12">本日より3日後以降でお願いします。<span class="inb">（木・日曜・祝日はお休み）</span></p>
									</div>
								</dd>
								<dt>予約日時（第2希望）</dt>
								<dd>
									<div>
										<input type="date" size="50" name="yoyakubi2_date" value="<?php echo $yoyakubi2_date; ?>">
										<select name="yoyakubi2_time">
											<option value="">時間</option>
											<option value="10:00">10:00</option>
											<option value="11:00">11:00</option>
											<option value="12:00">12:00</option>
											<option value="13:00">13:00</option>
											<option value="14:00">14:00</option>
											<option value="15:00">15:00</option>
											<option value="16:00">16:00</option>
											<option value="17:00">17:00</option>
											<option value="18:00">18:00</option>
										</select>
										<p class="fsize12">本日より3日後以降でお願いします。<span class="inb">（木・日曜・祝日はお休み）</span></p>
									</div>
								</dd>
								<dt>スタッフ指名</dt>
								<dd>
									<select name="staff">
										<option value="希望なし">希望なし</option>
										<option value="杉山 英二">杉山 英二（休み：木）</option>
										<option value="杉山 卓也">杉山 卓也（休み：木・金）</option>
										<option value="田名網 美菜子">田名網 美菜子 （休み：木・金・土　受付15時まで）</option>
									</select>
								</dd>
							</dl>
						</section>

						<div class="pet">
							<section class="box-karada">
								<h2 class="step">
									<span>5</span>
									ペットの情報を入力してください。
								</h2>
								<dl class="form-list">
									<dt>お名前</dt>
									<dd>
										<input type="text" name="pet_name" id="pet_name" value="<?php echo $pet_name; ?>" />
									</dd>
									<dt>種類</dt>
									<dd>
										<input type="text" name="pet_shurui" id="pet_shurui" placeholder="犬、猫など" value="<?php echo $pet_shurui; ?>" />
									</dd>
									<dt>種別</dt>
									<dd>
										<input type="text" name="pet_shubetsu" id="pet_shubetsu" value="<?php echo $pet_shubetsu; ?>" />
									</dd>
									<dt>年齢</dt>
									<dd>
										<input type="text" name="pet_nenrei" id="pet_nenrei" value="<?php echo $pet_nenrei; ?>" />
									</dd>
									<dt>体重</dt>
									<dd>
										<input type="text" name="pet_taijyuu" id="pet_taijyuu" value="<?php echo $pet_taijyuu; ?>" />（キロ）
									</dd>
								</dl>
							</section>
							<section class="box-karada">
								<h2 class="step">
									<span>6</span>
									お身体の状態を入力してください。
								</h2>
								<dl class="form-list">
									<dt>今一番気になる症状は？</dt>
									<dd>
										<textarea name="pet_shoujyou1" rows="8" cols="80"><?php echo $pet_shoujyou1; ?></textarea>
									</dd>
									<dt>いつ頃からですか？原因は？</dt>
									<dd>
										<textarea name="pet_shoujyou2" rows="8" cols="80"><?php echo $pet_shoujyou2; ?></textarea>
									</dd>
									<dt>その症状が酷くなるのは？（例、朝起きた時、疲れたとき、冬など）</dt>
									<dd>
										<textarea name="pet_shoujyou3" rows="8" cols="80"><?php echo $pet_shoujyou3; ?></textarea>
									</dd>
									<dt>今までに病気をしたことがありますか？いつ頃？</dt>
									<dd>
										<textarea name="pet_shoujyou4" rows="8" cols="80"><?php echo $pet_shoujyou4; ?></textarea>
									</dd>
								</dl>
							</section>
							<section class="box-taichou">
								<h2 class="step">
									<span>7</span>
									現在の体調にあてはまるものをチェックしてください。
								</h2>
								<dl class="form-list">
									<dt>食欲は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="pet_taichou1[]" type="checkbox" value="あまり食欲がない" />あまり食欲がない</label>
											<label><input name="pet_taichou1[]" type="checkbox" value="過剰に食べる" />過剰に食べる</label>
											<label><input name="pet_taichou1[]" type="checkbox" value="食べても吐いてしまう" />食べても吐いてしまう</label>
										</div>
									</dd>
									<dt>飲物は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="pet_taichou2[]" type="checkbox" value="よく水分を取る" />よく水分を取る</label>
											<label><input name="pet_taichou2[]" type="checkbox" value="あまり水分を摂らない" />あまり水分を摂らない</label>
										</div>
									</dd>
									<dt>便通は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="pet_taichou3[]" type="checkbox" value="軟便気味だ" />軟便気味だ</label>
											<label><input name="pet_taichou3[]" type="checkbox" value="便秘気味だ" />便秘気味だ</label>
											<label><input name="pet_taichou3[]" type="checkbox" value="よく下痢をする" />よく下痢をする</label>
										</div>
									</dd>
									<dt>小便は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="pet_taichou4[]" type="checkbox" value="トイレが近い（日中）" />トイレが近い（日中）</label>
											<label><input name="pet_taichou4[]" type="checkbox" value="トイレが近い（夜間）" />トイレが近い（夜間）</label>
											<label><input name="pet_taichou4[]" type="checkbox" value="色が透明だ" />色が透明だ</label>
											<label><input name="pet_taichou4[]" type="checkbox" value="色が濃い" />色が濃い</label>
											<label><input name="pet_taichou4[]" type="checkbox" value="あまり小便をしない" />あまり小便をしない</label>
										</div>
									</dd>
									<dt>体質は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="pet_taichou5[]" type="checkbox" value="疲れやすい" />疲れやすい</label>
											<label><input name="pet_taichou5[]" type="checkbox" value="むくみやすい" />むくみやすい</label>
											<label><input name="pet_taichou5[]" type="checkbox" value="よく吠える(夜)" />よく吠える(夜)</label>
											<label><input name="pet_taichou5[]" type="checkbox" value="よく吠える(日中)" />よく吠える(日中)</label>
											<label><input name="pet_taichou5[]" type="checkbox" value="円形脱毛がある" />円形脱毛がある</label>
											<label><input name="pet_taichou5[]" type="checkbox" value="鼻が乾いている" />鼻が乾いている</label>
										</div>
									</dd>
								</dl>
							</section>
							<section class="box-other">
								<h2 class="step">
									<span>8</span>
									その他
								</h2>
								<dl class="form-list">
									<dt>現在服用中の薬、漢方薬、サプリメント</dt>
									<dd>
										<textarea name="pet_etc1"><?php echo $pet_etc1; ?></textarea>
									</dd>
									<dt>処方する際、お望みの剤形</dt>
									<dd>
										<div class="check-list">
											<label><input name="pet_etc2[]" type="checkbox" value="錠剤">錠剤</label>
											<label><input name="pet_etc2[]" type="checkbox" value="散剤">散剤</label>
											<label><input name="pet_etc2[]" type="checkbox" value="液体">液体</label>
											<label><input name="pet_etc2[]" type="checkbox" value="とくに指定はない">とくに指定はない</label>
										</div>
									</dd>
								</dl>
							</section>
							<section class="box-mesu">
								<h2 class="step">
									<span>9</span>
									雌の場合は答えてください。
								</h2>
								<dl class="form-list">
									<dt>出産経験はありますか？</dt>
									<dd>
										<input name="pet_women1[]" type="text" size="5" maxlength="3" /> 年前　
										<input name="pet_women1[]" type="checkbox" value="ない" />ない
									</dd>
									<dt>生理の出血は？</dt>
									<dd>
										<div class="check-list">
										</div>
										<label><input name="pet_women2[]" type="checkbox" value="多い" />多い</label>
										<label><input name="pet_women2[]" type="checkbox" value="少ない" />少ない</label>
										<label><input name="pet_women2[]" type="checkbox" value="普通" />普通 </label>
									</dd>
									<dt>経血の状態は？</dt>
									<dd>
										<div class="check-list">
										</div>
										<label><input name="pet_women3[]" type="checkbox" value="どろっとした塊りがある" />どろっとした塊りがある</label>
										<label><input name="pet_women3[]" type="checkbox" value="うすくさらさらしている" />うすくさらさらしている</label>
										<label><input name="pet_women3[]" type="checkbox" value="色が茶または黒っぽい" />色が茶または黒っぽい</label>
										<label><input name="pet_women3[]" type="checkbox" value="色が赤く、塊はない" />色が赤く、塊はない</label>
									</dd>
								</dl>
							</section>
						</div>

						<div class="no-pet">
							<section class="box-karada">
								<h2 class="step">
									<span>5</span>
									お身体の状態を入力してください。
								</h2>
								<dl class="form-list">
									<dt>今一番気になる症状は？</dt>
									<dd>
										<textarea name="shoujyou1" rows="8" cols="80"><?php echo $shoujyou1; ?></textarea>
									</dd>
									<dt>いつ頃からですか？原因は？</dt>
									<dd>
										<textarea name="shoujyou2" rows="8" cols="80"><?php echo $shoujyou2; ?></textarea>
									</dd>
									<dt>その症状が酷くなるのは？（例、朝起きた時、疲れたとき、冬など）</dt>
									<dd>
										<textarea name="shoujyou3" rows="8" cols="80"><?php echo $shoujyou3; ?></textarea>
									</dd>
									<dt>今までに病気をしたことがありますか？いつ頃？</dt>
									<dd>
										<textarea name="shoujyou4" rows="8" cols="80"><?php echo $shoujyou4; ?></textarea>
									</dd>
								</dl>
							</section>
							<section class="box-taichou">
								<h2 class="step">
									<span>6</span>
									現在の体調にあてはまるものをチェックしてください。
								</h2>
								<dl class="form-list">
									<dt>身長</dt>
									<dd>
										<input type="text" name="taichou1" id="taichou1" size="5" maxlength="3" />（センチ）　
									</dd>
									<dt>体重</dt>
									<dd>
										<input type="text" name="taichou2" id="taichou2" size="5" maxlength="3" />（キロ）
									</dd>
									<dt>顔色は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="taichou3[]" type="checkbox" value="血色が悪く白っぽい" />血色が悪く白っぽい</label>
											<label><input name="taichou3[]" type="checkbox" value="のぼせがちで赤くほてっている" />のぼせがちで赤くほてっている</label>
											<label><input name="taichou3[]" type="checkbox" value="血行が悪くくすんでいる" />血行が悪くくすんでいる</label>
										</div>
									</dd>
									<dt>寒熱は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="taichou4[]" type="checkbox" value="顔や手足がほてる" />顔や手足がほてる</label>
											<label><input name="taichou4[]" type="checkbox" value="手足の先のみ冷える" />手足の先のみ冷える</label>
											<label><input name="taichou4[]" type="checkbox" value="顔がのぼせて足が冷える" />顔がのぼせて足が冷える</label>
											<label><input name="taichou4[]" type="checkbox" value="全身が冷える" />全身が冷える</label>
											<label><input name="taichou4[]" type="checkbox" value="どちらかというと冷える" />どちらかというと冷える</label>
											<label><input name="taichou4[]" type="checkbox" value="下半身が冷える" />下半身が冷える</label>
											<label><input name="taichou4[]" type="checkbox" value="どちらかというと暑がり" />どちらかというと暑がり</label>
										</div>
									</dd>
									<dt>汗は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="taichou5[]" type="checkbox" value="汗が多い" />汗が多い</label>
											<label><input name="taichou5[]" type="checkbox" value="寝汗をよくかく" />寝汗をよくかく</label>
											<label><input name="taichou5[]" type="checkbox" value="動くとすぐに汗をかく" />動くとすぐに汗をかく</label>
											<label><input name="taichou5[]" type="checkbox" value="季節を問わず汗かきだ" />季節を問わず汗かきだ</label>
										</div>
									</dd>
									<dt>睡眠は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="taichou6[]" type="checkbox" value="寝付きが悪い" />寝付きが悪い</label>
											<label><input name="taichou6[]" type="checkbox" value="眠りが浅いor夜中に目が醒める" />眠りが浅いor夜中に目が醒める</label>
											<label><input name="taichou6[]" type="checkbox" value="日中いつも眠い" />日中いつも眠い</label>
											<label><input name="taichou6[]" type="checkbox" value="食後すぐ眠くなる" />食後すぐ眠くなる</label>
											<label><input name="taichou6[]" type="checkbox" value="朝起きられない" />朝起きられない</label>
										</div>
									</dd>
									<dt>食欲は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="taichou7[]" type="checkbox" value="あまり食欲がない" />あまり食欲がない</label>
											<label><input name="taichou7[]" type="checkbox" value="食べると胃がもたれる" />食べると胃がもたれる</label>
											<label><input type="checkbox" value="食べてもすぐ腹が減る" />食べてもすぐ腹が減る</label>
											<label><input name="taichou7[]" type="checkbox" value="食欲はあるがあまり食べられない" />食欲はあるがあまり食べられない</label>
											<label><input name="taichou7[]" type="checkbox" value="よく胃痛がする" />よく胃痛がする</label>
										</div>
									</dd>
									<dt>飲物は？</dt>
									<dd>
										<label><input name="taichou8[]" type="checkbox" value="口やのどが渇いてよく水分をとる" />口やのどが渇いてよく水分をとる</label>
										<div class="check-list">
											<label><input name="taichou8[]" type="checkbox" value="あまり水分を摂らない" />あまり水分を摂らない</label>
											<label><input name="taichou8[]" type="checkbox" value="冷たい飲物が好き" />冷たい飲物が好き</label>
											<label><input name="taichou8[]" type="checkbox" value="暖かい飲み物が好き" />暖かい飲み物が好き</label>
											<label><input name="taichou8[]" type="checkbox" value="水分をとるように心がけている" />水分をとるように心がけている</label>
										</div>
									</dd>
									<dt>便通は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="taichou9[]" type="checkbox" value="便秘で薬を飲んでいる" />便秘で薬を飲んでいる</label>
											<label><input name="taichou9[]" type="checkbox" value="よく下痢をする" />よく下痢をする</label>
											<label><input name="taichou9[]" type="checkbox" value="腹が張ってよくガスが出る" />腹が張ってよくガスが出る</label>
											<label><input name="taichou9[]" type="checkbox" value="便秘気味だ" />便秘気味だ</label>
											<label><input name="taichou9[]" type="checkbox" value="軟便気味だ" />軟便気味だ</label>
										</div>
									</dd>
									<dt>小便は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="taichou10[]" type="checkbox" value="トイレが近い（日中）" />トイレが近い（日中）</label>
											<label><input name="taichou10[]" type="checkbox" value="トイレが近い（夜間）" />トイレが近い（夜間）</label>
											<label><input name="taichou10[]" type="checkbox" value="色が透明だ" />色が透明だ</label>
											<label><input name="taichou10[]" type="checkbox" value="色が濃い" />色が濃い</label>
											<label><input name="taichou10[]" type="checkbox" value="あまりトイレに行かない" />あまりトイレに行かない</label>
										</div>
									</dd>
									<dt>体質は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="taichou11[]" type="checkbox" value="疲れやすい" />疲れやすい</label>
											<label><input name="taichou11[]" type="checkbox" value="風邪をひきやすい" />風邪をひきやすい</label>
											<label><input name="taichou11[]" type="checkbox" value="貧血気味だ" />貧血気味だ</label>
											<label><input name="taichou11[]" type="checkbox" value="上半身（顔など）がむくみやすい" />上半身（顔など）がむくみやすい</label>
											<label><input name="taichou11[]" type="checkbox" value="肩こりor頭痛がする" />肩こりor頭痛がする</label>
											<label><input name="taichou11[]" type="checkbox" value="下半身（足など）がむくみやすい" />下半身（足など）がむくみやすい</label>
											<label><input name="taichou11[]" type="checkbox" value="神経質だ" />神経質だ</label>
											<label><input name="taichou11[]" type="checkbox" value="イライラしやすい" />イライラしやすい</label>
											<label><input name="taichou11[]" type="checkbox" value="腰や膝が痛いorだるい" />腰や膝が痛いorだるい</label>
											<label><input name="taichou11[]" type="checkbox" value="肌が荒れやすい" />肌が荒れやすい</label>
										</div>
									</dd>
									<dt>血圧は？</dt>
									<dd>
										<div>
											上 <input name="taichou12" type="text" id="64. 血圧は？" size="5" maxlength="3" /> ㎜Hg
											下 <input name="taichou13" type="text" id="65. 血圧は？" size="5" maxlength="3" /> ㎜Hg
										</div>
										<div>
											<label><input name="taichou14" type="checkbox" value="降圧剤を服用している" />降圧剤を服用している</label>
										</div>
									</dd>
								</dl>
							</section>
							<section class="box-bero">
								<h2 class="step">
									<span>7</span>
									舌の状態を教えてください。
								</h2>
								<p>あなたの状態に近いものを選んでください。<br>｢正常な舌｣の写真を参考にあてはまるものをすべてチェックしてください。</p>
								<dl class="bero-list">
									<dt>舌の色は？（表面に苔がついている場合は周辺の色を見てください）</dt>
									<dd>
										<ul>
											<li>
												<div class="berodesc">薄く赤みがかかり、鮮やかなピンク色</div>
												<div class="beropict">
													<a href="/common/images/bero/zoom/1.jpg" title="薄く赤みがかかり、鮮やかなピンク色"><img src="/common/images/bero/1.jpg" alt="" border="0" /></a></a>
												</div>
												<div class="berocheck">
													<label><input name="bero1[]" type="checkbox" value="正常な舌" />正常な舌</label>
												</div>
											</li>
											<li>
												<div class="berodesc">白っぽいピンク色</div>
												<div class="beropict">
													<a href="/common/images/bero/zoom/2.jpg" title="白っぽいピンク色"><img src="/common/images/bero/2.jpg" alt="" border="0" /></a>
												</div>
												<div class="berocheck">
													<label><input name="bero1[]" type="checkbox" value="淡白舌" />淡白舌</label>
												</div>
											</li>
											<li>
												<div class="berodesc">濃い赤色</div>
												<div class="beropict">
													<a href="/common/images/bero/zoom/3.jpg" title="濃い赤色"><img src="/common/images/bero/3.jpg" alt="" border="0" /></a>
												</div>
												<div class="berocheck">
													<label><input name="bero1[]" type="checkbox" value="紅舌" />紅舌</label>
												</div>
											</li>
											<li>
												<div class="berodesc">紫がかっているor紫の小さな斑点がある</div>
												<div class="beropict">
													<a href="/common/images/bero/zoom/4.jpg" title="紫舌"><img src="/common/images/bero/4.jpg" alt="" border="0" /></a>
												</div>
												<div class="berocheck">
													<label><input name="bero1[]" type="checkbox" value="紫舌" />紫舌</label>
												</div>
											</li>
										</ul>
									</dd>
									<dt>苔は？（どちらかに当てはまる場合のみチェックしてください。白い苔が薄くついているのは正常です）</dt>
									<dd>
										<ul>
											<li>
												<div class="beropict">
													<a href="/common/images/bero/zoom/5.jpg" title="白苔"><img src="/common/images/bero/5.jpg" alt="" border="0" /></a>
												</div>
												<div class="berocheck">
													<label><input name="bero2[]" type="checkbox" value="白苔" />白苔</label>
												</div>
											</li>
											<li>
												<div class="beropict">
													<a href="/common/images/bero/zoom/6.jpg" title="黄苔"><img src="/common/images/bero/6.jpg" alt="" border="0" /></a>
												</div>
												<div class="berocheck">
													<label><input name="bero2[]" type="checkbox" value="黄苔" />黄苔</label>
												</div>
											</li>
										</ul>
									</dd>
									<dt>下記の舌に当てはまる場合はチェックしてください。</dt>
									<dd>
										<ul>
											<li>
												<div class="berodesc">舌自体が赤く、ひび割れている</div>
												<div class="beropict">
													<a href="/common/images/bero/zoom/7.jpg" title="舌自体が赤く、ひび割れている"><img src="/common/images/bero/7.jpg" alt="" border="0" /></a>
												</div>
												<div class="berocheck">
													<label><input name="bero3[]" type="checkbox" value="裂紋舌" />裂紋舌</label>
												</div>
											</li>
											<li>
												<div class="berodesc">舌の裏側の静脈がうっ血していて、太い</div>
												<div class="beropict">
													<a href="/common/images/bero/zoom/8.jpg" title="舌の裏側の静脈がうっ血していて、太い"><img src="/common/images/bero/8.jpg" alt="" border="0" /></a>
												</div>
												<div class="berocheck">
													<label><input name="bero3[]" type="checkbox" value="舌下怒張" />舌下怒張</label>
												</div>
											</li>
										</ul>
									</dd>
								</dl>
							</section>
							<section class="box-other">
								<h2 class="step">
									<span>8</span>
									その他
								</h2>
								<dl class="form-list">
									<dt>現在服用中の薬、漢方薬、サプリメント</dt>
									<dd>
										<textarea name="etc1"><?php echo $etc1; ?></textarea>
									</dd>
									<dt>処方する際、お望みの剤形</dt>
									<dd>
										<div class="check-list">
											<label><input name="etc2[]" type="checkbox" value="錠剤">錠剤</label>
											<label><input name="etc2[]" type="checkbox" value="散剤">散剤</label>
											<label><input name="etc2[]" type="checkbox" value="液体">液体</label>
											<label><input name="etc2[]" type="checkbox" value="とくに指定はない">とくに指定はない</label>
										</div>
									</dd>
								</dl>
							</section>
							<section class="box-women">
								<h2 class="step">
									<span>9</span>
									女性の方は答えてください。
								</h2>
								<dl class="form-list">
									<dt>出産経験はありますか？</dt>
									<dd>
										<input name="women1[]" type="text" size="5" maxlength="3" /> 年前　
										<input name="women1[]" type="checkbox" value="ない" />ない
									</dd>
									<dt>生理の周期は？</dt>
									<dd>
										<div class="check-list">
											<label>生理の周期は（ <input name="women2[]" type="textbox" class="txtbox03" /> 日 ）</label>
											<label><input name="women2[]" type="checkbox" value="生理が一時的に止まっている" />生理が一時的に止まっている</label>
											<label><input name="women2[]" type="checkbox" value="閉経したor閉経しそうだ" />閉経したor閉経しそうだ</label>
											<label><input name="women2[]" type="checkbox" value="周期が安定していなくバラバラ" />周期が安定していなくバラバラ</label>
										</div>
									</dd>
									<dt>生理の出血は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="women3[]" type="checkbox" value="多い" />多い</label>
											<label><input name="women3[]" type="checkbox" value="少ない" />少ない</label>
											<label><input name="women3[]" type="checkbox" value="普通" />普通 </label>
										</div>
									</dd>
									<dt>生理の期間は？</dt>
									<dd>
										<div class="check-list">
											<label>生理の期間は（ <input name="women4" name="" type="textbox" id="" value="" class="txtbox03" /> 日 ）</label>
										</div>
									</dd>
									<dt>生理痛は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="women5[]" type="checkbox" value="痛みがひどいor鎮痛剤を服用している" />痛みがひどいor鎮痛剤を服用している</label>
											<label><input name="women5[]" type="checkbox" value="ある（薬は必要ない）" />ある（薬は必要ない）</label>
											<label><input name="women5[]" type="checkbox" value="ない" />ない</label>
										</div>
									</dd>
									<dt>経血の状態は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="women6[]" type="checkbox" value="どろっとした塊りがある" />どろっとした塊りがある</label>
											<label><input name="women6[]" type="checkbox" value="うすくさらさらしている" />うすくさらさらしている</label>
											<label><input name="women6[]" type="checkbox" value="色が茶または黒っぽい" />色が茶または黒っぽい</label>
											<label><input name="women6[]" type="checkbox" value="色が赤く、塊はない" />色が赤く、塊はない</label>
										</div>
									</dd>
									<dt>生理前後の状態は？</dt>
									<dd>
										<div class="check-list">
											<label><input name="women7[]" type="checkbox" value="生理前に胸が張ったり、イライラする" />生理前に胸が張ったり、イライラする</label>
											<label><input name="women7[]" type="checkbox" value="普段より眠い、疲れやすい" />普段より眠い、疲れやすい</label>
											<label><input name="women7[]" type="checkbox" value="ニキビが出る" />ニキビが出る</label>
											<label><input name="women7[]" type="checkbox" value="むくむ" />むくむ</label>
											<label><input name="women7[]" type="checkbox" value="下痢する" />下痢する</label>
											<label><input name="women7[]" type="checkbox" value="便秘する" />便秘する</label>
											<label><input name="women7[]" type="checkbox" value="特に症状はない" />特に症状はない</label>
										</div>
									</dd>
								</dl>
							</section>
						</div>
						<div class="note2">
							<p>
								ご相談内容の確認メールをお送りいたします。確認メールがが届かない場合はご相談メールが正しく送信されていない可能性がありますのでもう一度送信して頂くか、お電話でご連絡ください。
							</p>
							<p class="policy"><a href="/php/policy.html" target="_blank">個人情報の取り扱い</a></p>
						</div>
						<div class="btn">
							<input type="submit" name="submit" value="送信" class="js-button-submit" id="button-submit" />
						</div>
					</form>
				</div>
			</div>
			<!--mainContents-->
			<div id="subContents">
				<?php readfile("../subContents.html") ?>
			</div>
			<!--subContents-->
		</div>
		<!--contents-->
		<?php readfile("../footer.html") ?>
	</div>
</body>

</html>