<?php
define("DEBUG", 1); // 1:debug 0:not debug
define("ENC_INT", 'UTF-8'); // 内部文字コード
define("ENC_OUT", 'UTF-8'); // 出力文字コード

define("MAIL_FROM_ADDRESS", 'info@sugiyaku.com');
define("MAIL_TO_ADDRESS_ADMIN", 'info@sugiyaku.com,masumi.k-ph@sugiyaku.com,takuya0613@gmail.com');
// define("MAIL_TO_ADDRESS_ADMIN", 'ys@motif-xxx.com,d_vermouth@yahoo.co.jp');
define("MAIL_FROM_TEXT", '漢方のスギヤマ薬局');

ini_set('display_errors', 0);

mb_internal_encoding(ENC_INT);
mb_http_output(ENC_OUT);
mb_language("ja");

//フォームライブラリクラス
require_once("../assets/php/FormLib.php");
$FormLib = new FormLib();

$post = $FormLib->rhe($_POST, mb_internal_encoding());
unset($_POST);
$param = $FormLib->rhe($_GET, mb_internal_encoding());
unset($_GET);

//リファラーチェック
$host = $_SERVER['HTTP_REFERER'];
$referer = parse_url($host);
if (!stristr($referer['host'], "sugiyaku.com")) {
	if (DEBUG) {
		echo "エラー";
		echo $referer['host'];
	} else {
		header("Location: ./");
	}
	exit;
}

//日本語文字列が含まれているか？
$jpcheck_str = $post['name'] . $post['address-level1'] . $post['address-level2'];
if (!preg_match("/[一-龠]+|[ぁ-ん]+|[ァ-ヴー]+|[一-龠]+|[ａ-ｚＡ-Ｚ０-９]/u", $jpcheck_str)) {
	if (DEBUG) {
		echo "エラー";
		echo $jpcheck_str;
	} else {
		header("Location: ./");
	}
	exit;
}


if (empty($post) || empty($post['name']) || empty($post['email'])) {
	header("Location: ./");
	exit;
}

//チェックボックス選択アイテム取得
function GetCheckData($itemname)
{
	$ret = "";
	for ($i = 0; $i < count(@$itemname); $i++) {
		if ($i != 0) {
			$ret .= "・";
		}
		$ret .= $itemname[$i];
	}
	return $ret;
}


$inputData = <<< DATA
＜ご相談方法＞
{$post['houhou']}

＜ご相談される方＞
{$post['target']}

＜ご相談は初めてですか？＞
{$post['hajimete']}

＜基本情報＞

＿お名前
{$post['name']}

＿フリガナ
{$post['name-kana']}

＿性別
{$post['sex']}

＿年齢
{$post['age']}

＿郵便番号
{$post['postal-code']}

＿都道府県
{$post['address-level1']}

＿市区町村
{$post['address-level2']}

＿番地
{$post['address-line1']}

＿マンション名
{$post['address-line2']}

＿電話
{$post['tel']}

＿メールアドレス
{$post['email']}

＿スカイプID
{$post['skypeid']}

＿予約日時（第1希望）
{$post['yoyakubi1_date']} {$post['yoyakubi1_time']}

＿予約日時（第2希望）
{$post['yoyakubi2_date']} {$post['yoyakubi2_time']}

＿スタッフ指名
{$post['staff']}


DATA;

// ペット
if (strcmp($post['target'], "ペット") == 0) {

	$pet_taichou1 = GetCheckData($post['pet_taichou1']);
	$pet_taichou2 = GetCheckData($post['pet_taichou2']);
	$pet_taichou3 = GetCheckData($post['pet_taichou3']);
	$pet_taichou4 = GetCheckData($post['pet_taichou4']);
	$pet_taichou5 = GetCheckData($post['pet_taichou5']);
	$pet_etc2 = GetCheckData($post['pet_etc2']);
	$pet_women1 = GetCheckData($post['pet_women1']);
	$pet_women2 = GetCheckData($post['pet_women2']);
	$pet_women3 = GetCheckData($post['pet_women3']);

	$inputData .= <<< DATA
＜ペット情報＞

＿お名前
{$post['pet_name']}

＿種類
{$post['pet_shurui']}

＿種別
{$post['pet_shubetsu']}

＿年齢
{$post['pet_nenrei']}

＿体重
{$post['pet_taijyuu']}


＜お身体の状態＞

＿今一番気になる症状は？
{$post['pet_shoujyou1']}

＿いつ頃からですか？原因は？
{$post['pet_shoujyou2']}

＿その症状が酷くなるのは？
{$post['pet_shoujyou3']}

＿今までに病気をしたことがありますか？いつ頃？
{$post['pet_shoujyou4']}


＜現在の体調＞

＿食欲は？
{$pet_taichou1}

＿飲物は？
{$pet_taichou2}

＿便通は？
{$pet_taichou3}

＿小便は？
{$pet_taichou4}

＿体質は？
{$pet_taichou5}


＜その他＞

＿現在服用中の薬、漢方薬、サプリメント
{$post['pet_etc1']}

＿処方する際、お望みの剤形
{$pet_etc2}


＜雌の場合＞

＿出産経験はありますか？
{$pet_women1}

＿生理の出血は？
{$pet_women2}

＿経血の状態は？
{$pet_women3}

DATA;
} else {
	$taichou1 = GetCheckData($post['taichou1']);
	$taichou2 = GetCheckData($post['taichou2']);
	$taichou3 = GetCheckData($post['taichou3']);
	$taichou4 = GetCheckData($post['taichou4']);
	$taichou5 = GetCheckData($post['taichou5']);
	$taichou6 = GetCheckData($post['taichou6']);
	$taichou7 = GetCheckData($post['taichou7']);
	$taichou8 = GetCheckData($post['taichou8']);
	$taichou9 = GetCheckData($post['taichou9']);
	$taichou10 = GetCheckData($post['taichou10']);
	$taichou11 = GetCheckData($post['taichou11']);
	$taichou12 = GetCheckData($post['taichou12']);
	$taichou13 = GetCheckData($post['taichou13']);
	$taichou14 = GetCheckData($post['taichou14']);
	$bero1 = GetCheckData($post['bero1']);
	$bero2 = GetCheckData($post['bero2']);
	$bero3 = GetCheckData($post['bero3']);

	$etc2 = GetCheckData($post['etc2']);
	$women1 = GetCheckData($post['women1']);
	$women2 = GetCheckData($post['women2']);
	$women3 = GetCheckData($post['women3']);
	$women5 = GetCheckData($post['women5']);
	$women6 = GetCheckData($post['women6']);
	$women7 = GetCheckData($post['women7']);

	$inputData .= <<< DATA
＜お身体の状態＞

＿今一番気になる症状は？
{$post['shoujyou1']}

＿いつ頃からですか？原因は？
{$post['shoujyou2']}

＿その症状が酷くなるのは？
{$post['shoujyou3']}

＿今までに病気をしたことがありますか？いつ頃？
{$post['shoujyou4']}


＜現在の体調＞

＿身長
{$post['taichou1']}（センチ）

＿体重
{$post['taichou2']}（キロ）

＿顔色は？
{$taichou3}

＿寒熱は？
{$taichou4}

＿汗は？
{$taichou5}

＿睡眠は？
{$taichou6}

＿食欲は？
{$taichou7}

＿飲物は？
{$taichou8}

＿便通は？
{$taichou9}

＿小便は？
{$taichou10}

＿体質は？
{$taichou11}

＿血圧は？
上 {$post['taichou12']} 下 {$post['taichou13']} {$post['taichou14']}


＜舌の状態＞

＿舌の色は？
{$bero1}

＿苔は？
{$bero2}

＿当てはまる場合
{$bero3}


＜その他＞

＿現在服用中の薬、漢方薬、サプリメント
{$post['etc1']}

＿処方する際、お望みの剤形
{$etc2}


＜女性の方＞

＿出産経験はありますか？
{$women1}

＿生理の周期は？
{$women2}

＿生理の出血は？
{$women3}

＿生理の期間は？
{$post['women4']}日

＿生理痛は？
{$women5}

＿経血の状態は？
{$women6}

＿生理前後の状態は？
{$women7}

DATA;
}


// 送信者へ
$body = <<< BODY
{$post['name']} 様

ご相談予約ありがとうございます。
確認でき次第、予約時間についてメールかお電話にてご連絡させていただきます。
お急ぎの方は店舗に直接お電話にてご連絡ください。

【ご送信内容】

{$inputData}

----------------------------------------------------
漢方のスギヤマ薬局
〒 252-0001 座間市相模が丘5-10-37
TEL: 042-746-1951
営業時間: 9:00~19:00
定休日：木曜日、日曜日、祝日
https://www.sugiyaku.com
----------------------------------------------------
BODY;

$subject = "【漢方のスギヤマ薬局】ご相談予約（自動返信メール）";
$ret = $FormLib->sendMail($post['email'], $subject, $body, MAIL_FROM_TEXT, MAIL_FROM_ADDRESS, DEBUG);


// 管理者へ
$body = <<< BODY
ご相談予約を受け付けました。

{$inputData}

BODY;

$subject = "【漢方のスギヤマ薬局】ご相談予約";

if (DEBUG)
	$ret .= $FormLib->sendMail($post['email'], $subject, $body, MAIL_FROM_TEXT, $post['email'], DEBUG);
else
	$ret .= $FormLib->sendMail(MAIL_TO_ADDRESS_ADMIN, $subject, $body, MAIL_FROM_TEXT, $post['email'], DEBUG);

header("Location: ./thanks.php");
