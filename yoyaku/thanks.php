<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>ご相談予約 総合受付 | 漢方相談 スギヤマ薬局</title>
<?php readfile("../head.html")?>
<link rel="stylesheet" type="text/css" href="/assets/css/yoyaku.css" />
<script src="/assets/js/yoyaku.js"></script>
</head>
<body id="idx" class="under yoyaku">
<div id="container">
<?php readfile("../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>ご相談予約 総合受付</span></p>
		<div id="mainContents" class="clearfix">
			<div class="box1 thanks">
				<h2>送信しました。</h2>
				<p>ご相談予約ありがとうございます。 <br>確認でき次第、予約時間についてメールかお電話にてご連絡させていただきます。<br>お急ぎの方は店舗に直接お電話にてご連絡ください。</p>
			</div>
		</div>
		<!--mainContents-->
		<div id="subContents">
<?php readfile("../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->
<?php readfile("../footer.html")?>
</div>
</body>
</html>
