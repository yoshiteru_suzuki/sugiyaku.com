<?php

$now = strtotime(date('Y-m-d H:i'));

if ($now <= strtotime('2017-4-27 21:00')){
	$ttl = "最近何だかイライラする";
	$date = "4月27日（木）";
}else if ($now <= strtotime('2017-5-25 21:00')){
	$ttl = "初夏の胃腸病は漢方におまかせ";
	$date = "5月25日（木）";
}else if($now <= strtotime('2017-6-29 21:00')){
	$ttl = "夏に力を発揮する漢方";
	$date = "6月29日（木）";
}

if(empty($ttl)){
	$schedule = '<p class="mb05">準備中です。</p>';
}else{
	$schedule = '<p class="mb05">次回は、<em class="fb">'.$ttl.'</em>です。</p>';
	$schedule .= '<p>日程：'.$date.'　19:30～21:00 </p>';
}

// $schedule = '<p class="mb05">次回は、<em class="fb">花粉に負けない漢方</em>です。</p>';
// $schedule .= '<p>日程：3月30日（木）　19:30～21:00 </p>';

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>タクヤ先生の漢方・健康講座 | 漢方相談 スギヤマ薬局</title>
<?php readfile("../head.html")?>
<script>
$(function() {
	if(!smart)
	{
		$('#main_pic').crossSlide({
			sleep: 3,
			fade: 1
		}, [
			{ src: 'images/idx_visual1.jpg'},
//			{ src: 'images/idx_visual2.jpg'},
			{ src: 'images/idx_visual3.jpg'}
		]);
	}
});
</script>
</head>
<body id="idx" class="under">
<div id="container">
<?php readfile("../header.html")?>
	<div id="contents" class="inner clearfix">
		<p id="bread"><a href="/" id="bread_home">HOME</a><span>タクヤ先生の漢方・健康講座</span></p>
		<div id="mainContents" class="clearfix">
			<div id="main_visual">
				<h1 id="main_ttl"><img src="images/idx_ttl.png" alt="タクヤ先生の漢方・健康講座" /></h1>
				<div id="main_pic">loading..</div>
			</div>
			<div class="read" style="margin-top: 50px;">
				<p><a href="http://takuya-kanpo-consulting.com/seminar/" target="_blank">Takuya kanpo consulting</a> にて各種セミナー開催しております。</p>
			</div>
			<div class="book-content">
				<div class="box1">
					<h2>タクヤ先生が執筆した書籍です！</h2>
					<div class="book-wrap">
						<div class="img">
							<a href="https://amzn.to/2SWQ9LD" target="_blank">
								<img src="http://www.sugiyaku.com/course/images/book2.jpg" alt="" class="book">
							</a>
						</div>
						<div class="txt">
							<h3>現場で使える <br>薬剤師・登録販売者のための漢方相談便利帖 <br>わかる！選べる！漢方薬163</h3>
							<p>医療用・薬局・薬店で販売されている漢方薬のほぼ全てを網羅！どの参考書よりもわかりやすい解説書です！</p>
							<div class="link">
								<a href="https://amzn.to/2SWQ9LD" target="_blank">
									<img src="http://www.sugiyaku.com/course/images/btn-amazon.svg" alt="" class="btn">
								</a>
							</div>
						</div>
					</div>
					<div class="book-wrap">
						<div class="img">
							<a href="http://amzn.asia/3qYJkg6" target="_blank">
								<img src="http://www.sugiyaku.com/course/images/book.jpg" alt="" class="book">
							</a>
						</div>
						<div class="txt">
							<h3>現場で使える <br>薬剤師・登録販売者のための漢方相談便利帖 </h3>
							<p>漢方の勉強の方法、資格保有者（薬剤師・登録販売者等）が漢方を店舗で取り扱う方法をわかりやすく解説しています。 <br>一般の方でもわかりやすく漢方の知識が学べる一冊です！</p>
							<div class="link">
								<a href="http://amzn.asia/3qYJkg6" target="_blank">
									<img src="http://www.sugiyaku.com/course/images/btn-amazon.svg" alt="" class="btn">
								</a>
							</div>
						</div>
					</div>
				</div>

			</div>


			<!-- <div class="read">
				<p>漢方をきちんと扱いたい、確かな知識を得たい 漢方を楽しく、真剣に学びたい方に。</p>
				<p>人が健康になるために、人が病気を治すために、「本当」の知識をお教えします。</p>
			</div>
			<div class="box1 mb30">
				<div class="kanpou" id="suchedule">
					<?php //echo $schedule; ?>
				</div>
			</div>

			<section class="cont1">
				<h1>講義場所</h1>
				<p>
				場所：<a href="http://www.racal-odasaga.com/" target="_blank">ラクアルおださが</a> 4F「おださがぷらざ」
				<br />
				定員：40名　毎回ほぼ満席となりますのでできるだけお早めのご予約をお願いいたします。
				</p>
			</section>
			<section class="cont1">
				<h1>参加費</h1>
				<p>
					1,000円
				</p>
			</section>

			<section class="cont1">
				<h1>お申込み</h1>
				<p>講義前日までにお電話にて「タクヤ先生の漢方・健康講座」へお申し込みください。<br />電話番号：042-746-1951(本店) </p>
			</section>

			<section class="cont1">
				<p>「神奈川県薬剤師会生涯学習研修認定シール」ならびに「日本薬剤師会研修受講シール」それぞれ1単位を獲得できます。</p>
			</section>

			<div class="pic">
				<img src="images/idx_pic01.jpg" alt="タクヤ先生" />
				<a href="../info/staff.html#takuyasensei">講師：杉山卓也</a>
			</div>
			<div class="data">
				<p>漢方薬は本当の意味で病気を「治す」ことのできるもの。私自身も毎日、皆様からのお悩みに漢方を使うたびにその素晴らしい効果に触れています。ただし漢方薬を用いるには正しい知識が必要です。しかし現代、医療の臨床現場でも漢方の正しい知識をもっている方は残念ながら本当にごくわずか。さらに漢方のセミナーというのはどれも目が飛び出るほどの金額を取るものがほとんど。これでは漢方の素晴らしさが広がることは難しいのではないでしょうか。</p>
				<p>そこで、漢方をきちんと扱いたい、確かな知識を得たいという方に基礎からしっかりとお教えさせていただきたいという思いから場代なやテキスト費などの最低限度をまかなえる金額でこのような講義を企画させていただきました。</p>
				<p>
				漢方を楽しく、真剣に学びたい方であればどなたでも大歓迎です。
				ご興味のある会だけでももちろん結構ですが、順を追って漢方の理論や用語も取り込んでお話させていただき、確かな知識が身につくように考えさせていただいておりますので、どうぞ奮ってご参加くださいませ。</p>
				<img src="images/idx_illust01.gif" class="illust" alt="えんぴつくん" />
				<p>また、「健康講座」では漢方に固執せず、現代社会における健康の作り方を正しい知識と共にわかりやすく、かつ目で見て楽しめるような実験を取り入れたりすることで楽しくお話させていただきます。<br />漢方は難しそうで・・という方は是非こちらにご参加くださいませ。</p>
				<p>これらの講座によって皆様の健康知識づくりに少しでも貢献できればこれほど嬉しいことはありません。皆様のご参加を心よりお待ちしております。</p>
			</div> -->

		</div>
		<!--mainContents-->

		<div id="subContents">
<?php readfile("../subContents.html")?>
		</div>
		<!--subContents-->
	</div>
	<!--contents-->

<?php readfile("../footer.html")?>
</div>
</body>
</html>
